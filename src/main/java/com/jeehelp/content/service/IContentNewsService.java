package com.jeehelp.content.service;

import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.content.entity.ContentNews;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 文章内容 服务类
 * </p>
 *
 * @author xj
 * @since 2017-07-27
 */
public interface IContentNewsService extends IService<ContentNews> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo,ContentNews contentNews);
}
