package com.jeehelp.content.service.impl;


import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.content.entity.ContentNews;
import com.jeehelp.content.dao.ContentNewsMapper;
import com.jeehelp.content.service.IContentNewsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

/**
 * <p>
 * 文章内容 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-07-27
 */
@Service
public class ContentNewsServiceImpl extends ServiceImpl<ContentNewsMapper, ContentNews> implements IContentNewsService {
	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo,ContentNews contentNews) {
		Page<ContentNews> page = new Page<ContentNews>(pageInfo.getPage(), pageInfo.getPageSize());

		EntityWrapper<ContentNews> wrapper = new EntityWrapper<ContentNews>(contentNews);
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));
		 
		selectPage(page, wrapper);

		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}
}
