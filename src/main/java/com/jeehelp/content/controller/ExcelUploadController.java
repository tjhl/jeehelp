package com.jeehelp.content.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jeehelp.commons.base.BaseController;
import com.jeehelp.commons.report.excel.EasyExcel;

/**
 * excel 批量导入
 * @author xj
 * 2017年8月7日
 */
@Controller
@RequestMapping("/backgd/content/excel")
public class ExcelUploadController extends BaseController{
	
	/**
	 * 上传路径
	 */
	@Value("${upload.basedir}")
	public String basedir;

	 
	@RequestMapping("/toAdd") 
	public String toAdd(Model model){  
		return "/bg/content/excel/add";
	}
	
	@RequestMapping("/upLoad")
	@ResponseBody
	public Object upLoad(@RequestParam("file") MultipartFile file) {
		try {
			EasyExcel fastExcel = new EasyExcel(file.getInputStream());
			fastExcel.setSheetName("Sheet1");
//			List<MyTest> tests = fastExcel.parse(MyTest.class);
//			if(null != tests && !tests.isEmpty()) {
//				for(MyTest myTest : tests) {
//					System.out.println(JsonUtils.toJson(myTest));
//				}
//			} else {
//				System.out.println("没有结果");
//			}
			fastExcel.close();
			
			return renderSuccess("导入成功！");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
