package com.jeehelp.content.controller;

import javax.validation.Valid;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.content.entity.ContentNews;
import com.jeehelp.content.service.IContentNewsService;
import com.jeehelp.commons.base.BaseController;

/**
 * <p>
 * 文章内容 前端控制器
 * </p>
 *
 * @author xj
 * @since 2017-07-27
 */
@Controller
@RequestMapping("/backgd/content/contentNews")
public class ContentNewsController extends BaseController {

    @Autowired private IContentNewsService contentNewsService;
    
     /**
     * 进入分页页面
     * @return
     */
    @RequestMapping("/toList")
    public String toList() {
        return "/bg/content/contentNews/list";
    }
   
    /**
     * 查询分页数据
     * @return
     */
    @RequiresPermissions("content:contentNews:list")  
    @RequestMapping("/list")
    @ResponseBody
    public Object list(ContentNews contentNews,PageInfo pageInfo) {
       
        pageInfo.setSort("id");
        pageInfo.setOrder("desc");
        
        contentNewsService.selectDataGrid(pageInfo,contentNews);
        return renderPageData(pageInfo.getRows(), pageInfo.getTotal());
    }
    
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/toAdd")
    public String toAdd() {
        return "/bg/content/contentNews/add";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @RequiresPermissions("content:contentNews:add") 
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid ContentNews contentNews) {
        contentNews.setCreateDate(new Date());
        contentNews.setCreateUser(getUserId());
        boolean b = contentNewsService.insert(contentNews);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }
     
    /**
     * 编辑页面
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/toUpdate")
    public String toUpdate(Model model, Long id) {
        ContentNews contentNews = contentNewsService.selectById(id);
        model.addAttribute("obj", contentNews);
        return "/bg/content/contentNews/update";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @RequiresPermissions("content:contentNews:update")  
    @PostMapping("/update")
    @ResponseBody
    public Object update(@Valid ContentNews contentNews) {
        contentNews.setModifiedDate(new Date());
        boolean b = contentNewsService.updateById(contentNews);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
    
     /**
     * 删除
     * @param id
     * @return
     */
    @RequiresPermissions("content:contentNews:delete")   
    @PostMapping("/delete")
    @ResponseBody
    public Object delete(String ids) {
        boolean b = contentNewsService.deleteBatchIds(java.util.Arrays.asList(ids.split(",")));
        if (b) {
            return renderSuccess("删除成功！");
        } else {
            return renderError("删除失败！");
        }
    }
}
