package com.jeehelp.content.dao;

import com.jeehelp.content.entity.ContentNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 文章内容 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-07-27
 */
public interface ContentNewsMapper extends BaseMapper<ContentNews> {

}