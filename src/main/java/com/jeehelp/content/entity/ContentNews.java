package com.jeehelp.content.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jeehelp.commons.json.DictStringSerialize;
import com.jeehelp.commons.utils.GlobalData;

/**
 * <p>
 * 文章内容
 * </p>
 *
 * @author xj
 * @since 2017-07-27
 */
@TableName("content_news")
public class ContentNews extends Model<ContentNews> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 标题
     */
	private String title;
    /**
     * 图片路径
     */
	@TableField("pic_url")
	private String picUrl;
    /**
     * 内容
     */
	private String remark;
    /**
     * 是否删除0未删除 1删除
     */
	private Integer isdel;
    /**
     * 创建时间
     */
	@TableField("create_date")
	private Date createDate;
    /**
     * 创建人
     */
	@TableField("create_user")
	private Long createUser;
    /**
     * 修改时间
     */
	@TableField("modified_date")
	private Date modifiedDate;
    /**
     * 修改人
     */
	@TableField("modified_user")
	private Long modifiedUser;
    /**
     * 类型 字典表 对应字典类型content_type
     */
	private String type;
 
	/**
     * 类型 字典表 对应字典类型content_type
     */
	@TableField(exist=false)
	private String typeText;
	
	@JsonSerialize(using = DictStringSerialize.class)
	public String getTypeText() {
		return type+GlobalData.DICT_SPLIT+"content_type";
	}
 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getIsdel() {
		return isdel;
	}

	public void setIsdel(Integer isdel) {
		this.isdel = isdel;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Long getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getModifiedUser() {
		return modifiedUser;
	}

	public void setModifiedUser(Long modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
