package com.jeehelp.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页
 * @author xj
 * 2017年9月10日
 */
@Controller
@RequestMapping("/mp")
public class IndexController {
 
	@RequestMapping("/index")
	public String index(Model model) {
		return "/wap/index";
	}

}
