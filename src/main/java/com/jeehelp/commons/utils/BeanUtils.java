package com.jeehelp.commons.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Objects;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cglib.beans.BeanGenerator;
import org.springframework.cglib.beans.BeanMap;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.lang.StringUtils;
/**
 * bean 工具
 * 
 * @author xinjun
 *
 *         2017年8月25日
 */
public final class BeanUtils extends org.springframework.beans.BeanUtils {
	private BeanUtils() {
	}

	/**
	 * 实例化对象
	 * 
	 * @param clazz
	 *            类
	 * @return 对象
	 */
	@SuppressWarnings("unchecked")
	public static <T> T newInstance(Class<?> clazz) {
		return (T) instantiate(clazz);
	}

	/**
	 * 实例化对象
	 * 
	 * @param clazzStr
	 *            类名
	 * @return 对象
	 */
	public static <T> T newInstance(String clazzStr) {
		try {
			Class<?> clazz = Class.forName(clazzStr);
			return newInstance(clazz);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 获取Bean的属性
	 * 
	 * @param bean
	 *            bean
	 * @param propertyName
	 *            属性名
	 * @return 属性值
	 */
	public static Object getProperty(Object bean, String propertyName) {
		PropertyDescriptor pd = getPropertyDescriptor(bean.getClass(), propertyName);
		if (pd == null) {
			throw new RuntimeException(
					"Could not read property '" + propertyName + "' from bean PropertyDescriptor is null");
		}
		Method readMethod = pd.getReadMethod();
		if (readMethod == null) {
			throw new RuntimeException("Could not read property '" + propertyName + "' from bean readMethod is null");
		}
		if (!readMethod.isAccessible()) {
			readMethod.setAccessible(true);
		}
		try {
			return readMethod.invoke(bean);
		} catch (Throwable ex) {
			throw new RuntimeException("Could not read property '" + propertyName + "' from bean", ex);
		}
	}

	/**
	 * 设置Bean属性
	 * 
	 * @param bean
	 *            bean
	 * @param propertyName
	 *            属性名
	 * @param value
	 *            属性值
	 */
	public static void setProperty(Object bean, String propertyName, Object value) {
		PropertyDescriptor pd = getPropertyDescriptor(bean.getClass(), propertyName);
		if (pd == null) {
			throw new RuntimeException(
					"Could not set property '" + propertyName + "' to bean PropertyDescriptor is null");
		}
		Method writeMethod = pd.getWriteMethod();
		if (writeMethod == null) {
			throw new RuntimeException("Could not set property '" + propertyName + "' to bean writeMethod is null");
		}
		if (!writeMethod.isAccessible()) {
			writeMethod.setAccessible(true);
		}
		try {
			writeMethod.invoke(bean, value);
		} catch (Throwable ex) {
			throw new RuntimeException("Could not set property '" + propertyName + "' to bean", ex);
		}
	}

	/**
	 * 给一个Bean添加字段
	 * 
	 * @param superBean
	 *            父级Bean
	 * @param props
	 *            新增属性
	 * @return {Object}
	 */
	public static Object generator(Object superBean, BeanProperty... props) {
		Class<?> superclass = superBean.getClass();
		Object genBean = generator(superclass, props);
		BeanUtils.copy(superBean, genBean);
		return genBean;
	}

	/**
	 * 给一个class添加字段
	 * 
	 * @param superclass
	 *            父级
	 * @param props
	 *            新增属性
	 * @return {Object}
	 */
	public static Object generator(Class<?> superclass, BeanProperty... props) {
		BeanGenerator generator = new BeanGenerator();
		generator.setSuperclass(superclass);
		generator.setUseCache(true);
		for (BeanProperty prop : props) {
			generator.addProperty(prop.getName(), prop.getType());
		}
		return generator.create();
	}

	/**
	 * copy 对象属性到另一个对象，默认不使用Convert
	 * 
	 * @param src
	 * @param clazz
	 *            类名
	 * @return T
	 */
	public static <T> T copy(Object src, Class<T> clazz) {
		BeanCopier copier = BeanCopier.create(src.getClass(), clazz, false);

		T to = newInstance(clazz);
		copier.copy(src, to, null);
		return to;
	}

	/**
	 * 拷贝对象
	 * 
	 * @param src
	 *            源对象
	 * @param dist
	 *            需要赋值的对象
	 */
	public static void copy(Object src, Object dist) {
		BeanCopier copier = BeanCopier.create(src.getClass(), dist.getClass(), false);

		copier.copy(src, dist, null);
	}

	/**
	 * 将对象装成map形式
	 * 
	 * @param src
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Map toMap(Object src) {
		return BeanMap.create(src);
	}

	/**
	 * 将map 转为 bean
	 */
	public static <T> T toBean(Map<String, Object> beanMap, Class<T> valueType) {
		T bean = BeanUtils.newInstance(valueType);
		PropertyDescriptor[] beanPds = getPropertyDescriptors(valueType);
		for (PropertyDescriptor propDescriptor : beanPds) {
			String propName = propDescriptor.getName();
			// 过滤class属性
			if (propName.equals("class")) {
				continue;
			}
			if (beanMap.containsKey(propName)) {
				Method writeMethod = propDescriptor.getWriteMethod();
				if (null == writeMethod) {
					continue;
				}
				Object value = beanMap.get(propName);
				if (!writeMethod.isAccessible()) {
					writeMethod.setAccessible(true);
				}
				try {
					writeMethod.invoke(bean, value);
				} catch (Throwable e) {
					throw new RuntimeException("Could not set property '" + propName + "' to bean", e);
				}
			}
		}
		return bean;
	}

	/**
	 * 
	 * Map转换层Bean，使用泛型免去了类型转换的麻烦。
	 * 
	 * @param <T>
	 * @param map
	 * @param class1
	 * @return
	 */
	public static <T> T map2Bean(Map<String, Object> map, Class<T> class1) {
		T bean = null;
		try {
			bean = class1.newInstance();
			
		    ConvertUtils.register(new Converter()  
	        {  
	               
	            @SuppressWarnings("rawtypes")  
	            @Override  
	            public Object convert(Class arg0, Object arg1)  
	            {  
	                if(arg1 == null)  
	                {  
	                    return null;  
	                }  
	                if(!(arg1 instanceof String))  
	                {  
	                    throw new ConversionException("只支持字符串转换 !");  
	                }  
	                String str = (String)arg1;  
	                if(str.trim().equals(""))  
	                {  
	                    return null;  
	                }  
	                   
	                SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	                   
	                try{  
	                    return sd.parse(str);  
	                }  
	                catch(Exception e)  
	                {  
	                    throw new RuntimeException(e);  
	                }  
	                   
	            }  
	               
	        }, java.util.Date.class);  
	        
			org.apache.commons.beanutils.BeanUtils.populate(bean, map);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return bean;
	}

	/**
	 * 
	 * @Title: filterBeanPropertyEmptyToNull
	 * @Description: 将object对象中字符串类型的属性进行转换，如果值为空则设置成null
	 * @param obj
	 *            目标对象
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @date 2017年9月13日 下午3:34:58
	 * @author yubin
	 */
	public static void filterBeanPropertyEmptyToNull(Object obj)
			throws IllegalArgumentException, IllegalAccessException {
		if (obj != null) {
			Field[] fields = obj.getClass().getDeclaredFields();
			for (Field field : fields) {
				String name = field.getType().getName();
				if (StringUtils.equals(name, "java.lang.String")) {
					field.setAccessible(true);
					Object value = field.get(obj);
					String val = Objects.toString(value);
					if (StringUtils.isBlank(val)) {
						field.set(obj, null);
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: filterBeanPropertyEmptyToNull
	 * @Description: 将object对象中字符串类型的属性进行转换，如果值为空则设置成null
	 * @param obj
	 *            目标对象
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @date 2017年9月13日 下午3:34:58
	 * @author yubin
	 */
	public static void filterBeanPropertyEmptyToNull(Object obj, String... fieldNames)
			throws IllegalArgumentException, IllegalAccessException {
		if (obj != null) {
			Field[] fields = obj.getClass().getDeclaredFields();
			
			/***************判断字段名数组是否为空，置标志位 Start**************/
			boolean checkFieldFlag = false;
			if (fieldNames != null && fieldNames.length > 0) {
				checkFieldFlag = true;
			}
			/***************判断字段名数组是否为空，置标志位 End**************/
			
			for (Field field : fields) {
				//字段名称
				String name = field.getName();
				//字段类型名称
				String typeName = field.getType().getName();
				//是否需要过滤
				boolean isFilter = false;
				//如果过滤字段名数组不为空，则需要有针对的进行空串过滤操作
				if (checkFieldFlag) {
					//循环比对当前字段是否是需要进行过滤的字段，如果是，置标志位为true
					for (String fieldName : fieldNames) {
						if (StringUtils.equals(fieldName, name)) {
							isFilter = true;
							break;
						}
					}
				} else {
					//如果是全部过滤，即针对过滤的字符串数组为空，则直接置标志位为true
					isFilter = true;
				}
				//如果需要过滤并且字段类型是字符串，则判断字符串是否为空串，如果是空串，则给null
				if (isFilter && StringUtils.equals(typeName, "java.lang.String")) {
					field.setAccessible(true);
					Object value = field.get(obj);
					String val = Objects.toString(value);
					if (StringUtils.isBlank(val)) {
						field.set(obj, null);
					}
				}
			}
		}
	}
}
