package com.jeehelp.commons.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Common {
	 
	/**
	 * inputstream 转换 string bufferreader方式
	 * 
	 * @param is
	 * @return
	 */
	public static String isToStr(InputStream is) {

		BufferedReader reader=null;
		try {
			reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		StringBuilder sb = new StringBuilder();

		String line = null;

		try {
 
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/**
	 * inputstream 转换 string byte方式
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static String isToStrByte(InputStream in) throws IOException {
		StringBuffer out = new StringBuffer();
		byte[] b = new byte[4096];
		for (int n; (n = in.read(b)) != -1;) {
			out.append(new String(b, 0, n));
		}
		return out.toString();
	}

	/**
	 * inputstream 转换 string byteArrayOutputStream方式
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static String isToStrByteArray(InputStream is) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = -1;
		while ((i = is.read()) != -1) {
			baos.write(i);
		}
		return baos.toString();
	}

	/**
	 * 获取全球唯一标示GUID
	 * 
	 * @date 2011-5-25
	 * @return
	 */
	public static String getUUID() {
		
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
   
	/**
	 * 创建文件/文件夹
	 * 
	 * @param filepath
	 *            路径
	 * @param filename
	 *            文件名
	 * @param isDirectory
	 *            是否是文件夹
	 * @date 2011-5-25
	 * @return
	 * @throws IOException
	 */
	public static boolean createFile(String filepath, String filename,
			boolean isDirectory) throws IOException {
		filepath += filename;
		File file = new File(filepath);
		if (file.exists()) {
			return false;
		} else {
			if (isDirectory) {// 创建文件夹
				return file.mkdir();
			} else
				return file.createNewFile();
		}
	}

	/**
	 * 返回文件扩展�? * @param file 文件对象
	 * 
	 * @date 2011-5-25
	 * @return
	 */
	public static String getExtension(File file) {
		String filename = file.getName();
		return getExtension(filename);
	}

	/**
	 * 返回文件扩展�? * @param file 文件名称
	 * 
	 * @date 2011-5-25
	 * @return
	 */
	public static String getExtension(String filename) {
		if ((filename != null) && (filename.length() > 0)) {
			int i = filename.lastIndexOf('.');
			if ((i > -1) && (i < (filename.length()))) {
				return filename.substring(i, filename.length());
			}
		}
		return null;
	}

	/**
	 * 返回当前时间
	 * 
	 * @param fmt
	 *            格式�?默认格式:yyyy-MM-dd hh:mm:ss
	 * @return
	 */
	public static String getNowDate(String fmt) {
		if (fmt == null || "".equals(fmt))
			fmt = "yyyy-MM-dd hh:mm:ss";
		SimpleDateFormat format = new SimpleDateFormat(fmt);
		return format.format(new Date());
	}

	/**
	 * 获取随机�? * @param numlength 随机数位�? * @return
	 */
	public static String getRandom(int numlength) {
		Double rdm = Math.random() * Math.pow(10, numlength);
		String rs = rdm.intValue() + "";
		if (rs.length() < numlength)
			return getRandom(numlength);
		return rs;
	}

	public static int getRandomScope(int min, int max) {
		Random random = new Random();
		int s = random.nextInt(max) % (max - min + 1) + min;
		return s;
	}

	/**
	 * 获取随机�?默认为当前时�?yyyyMMddhhmmss) + 4位随机数
	 * 
	 * @return
	 */
	public static String getRandom() {
		return getNowDate("yyyyMMddhhmmss") + getRandom(4);
	}

	/**
	 * 前台alert字符
	 * 
	 * @param isback
	 * @param url
	 * @return
	 */
	public static String getAlertString(String msg, boolean isback, String url) {
		StringBuffer sb = new StringBuffer("");
		sb.append("<script>");
		sb.append("alert('" + msg + "');");
		if (isback)
			sb.append("history.go(-1)");
		else if (url != null && !"".equals(url))
			sb.append("top.location.href='" + url + "'");
		sb.append("</script>");
		return sb.toString();
	}
	/**
	 * 前台跳转
	 * 
	 * @param isback
	 * @param url
	 * @return
	 */
	public static String getLocationString(String url) {
		StringBuffer sb = new StringBuffer("");
		sb.append("<script>"); 
		if (url != null && !"".equals(url))
			sb.append("location.href='" + url + "'");
		sb.append("</script>");
		return sb.toString();
	}
	/**
	 * 如果字符串超出maxLength则截�? * @param str
	 * 
	 * @param maxLength
	 * @return
	 */
	public static String EnsureMaximumLength(String str, int maxLength) {
		if (str == null)
			return "";
		if (str.length() > maxLength) {
			return str.substring(maxLength);
		}
		return str;
	}

	/**
	 * 判断字符串是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotEmptyString(String str) {
		if (str == null) {
			return false;
		}
		if (str.trim().equals("")) {
			return false;
		}
		return true;
	}

	/**
	 * new String(str.getBytes("ISO-8859-1"), "utf-8");
	 * 
	 * @param str
	 * @return
	 */
	public static String getNewStr(String str) {

		if (isNotEmptyString(str)) {
			try {
				return new String(str.getBytes("ISO-8859-1"), "utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 对比Integer �?int
	 * 
	 * @param v
	 * @param cv
	 * @return
	 */
	public static boolean compareInteger(Integer v, int cv) {
		if (v == null)
			return false;
		if (v == cv)
			return true;
		return false;
	}

	/**
	 * 判断 数组是否为空
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isEmptyArray(Object[] obj) {
		if (obj != null && obj.length > 0)
			return true;
		return false;
	}

	/**
	 * 判断字符串是否为�? * @param str
	 * 
	 * @return
	 */
	public static boolean isEmptyString(String str) {
		return !isNotEmptyString(str);
	}

	/**
	 * 把NULL字符串转为空""
	 * 
	 * @param str
	 * @return
	 */
	public static String isNullToEmpty(String str) {
		if (str == null || "null".equals(str)) {
			str = "";
		}
		return str;
	}

	/**
	 * 获取cookie
	 * 
	 * @param name
	 * @return
	 */
	public static Cookie getCookie(HttpServletRequest request, String name) {
		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie c = cookies[i];

				if (c.getName().equals(name)) {
					return c;
				}
			}
		}

		return null;
	}

	/**
	 * 添加cookie
	 * 
	 * @param response
	 * @param name
	 * @param value
	 */
	public static void addCookie(HttpServletResponse response, String name,
			String value) {
		int maxAge = 3600 * 24 * 365;

		if (value == null) {
			maxAge = 0;
			value = "";
		}

		Cookie cookie = new Cookie(name, value);
		cookie.setMaxAge(maxAge);
		cookie.setPath("/");

		response.addCookie(cookie);
	}

	/**
	 * 获取writer
	 * 
	 * @param response
	 * @return
	 */
	public static PrintWriter getWriter(HttpServletResponse response) {
		try {
			return response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取session中的�? * @param request
	 * 
	 * @param vname
	 * @return
	 */
	public static Object getSessionAttribute(HttpServletRequest request,
			String vname) {
		HttpSession session = request.getSession();
		Object obj = session.getAttribute(vname);
		return obj;
	}

	/**
	 * 转换int
	 * 
	 * @param str
	 *            �?��转换的字符串
	 * @param default_val
	 *            默认�? * @return
	 */
	public static int parseInt(String str, int default_val) {
		try {
			int i = Integer.parseInt(str);
			return i;
		} catch (NumberFormatException e) {
		}
		return default_val;
	}

	/**
	 * 转换long
	 * 
	 * @param str
	 *            �?��转换的字符串
	 * @param default_val
	 *            默认�? * @return
	 */
	public static long parseLong(String str, long default_val) {

		try {
			long i = Long.parseLong(str);
			return i;
		} catch (NumberFormatException e) {
		}
		return default_val;
	}

	/**
	 * 转换城Integer
	 * 
	 * @param str
	 * @param default_val
	 * @return
	 */
	public static Integer parseInteger(String str, Integer default_val) {
		try {
			int i = Integer.parseInt(str);
			return i;
		} catch (NumberFormatException e) {
		}
		return default_val;
	}

	/**
	 * 转换url关符�? * @param str
	 * 
	 * @return
	 */
	public static String urlConvert(String str) {
		if (str != null) {
			return str.replace(" ", "+").replace("%2B", " ")
					.replace("%2F", "/").replace("%3F", "?")
					.replace("%25", "%").replace("%23", "").replace("%26", "&")
					.replace("%3D", "#");
		}
		return str;
	}

	/**
	 * jquery判断是否是ajax请求
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isAjaxRquest(HttpServletRequest request) {
		String head = request.getHeader("X-Requested-With");
		if (head != null && head.equalsIgnoreCase("XMLHttpRequest")) {
			return true;
		}
		return false;
	}

	/**
	 * 获取客户端ip
	 * 
	 * @param request
	 * @return
	 */
	public static String getClientIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Forwarded-For");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if (null != ip && ip.indexOf(',') != -1) {
			String[] ips = ip.split(",");
			for (int i = 0; i < ips.length; i++) {
				if (null != ips[i] && !"unknown".equalsIgnoreCase(ips[i])) {
					ip = ips[i];
					break;
				}
			}

		}
		return ip;
	}
	/**
	 * 查询用户浏览器信息
	 * @param request
	 * @return
	 *//*
    public static AgentClientVo getClient(HttpServletRequest request){
    	String Agent = request.getHeader("User-Agent");  
    	StringTokenizer st = new StringTokenizer(Agent,";"); 
    	st.nextToken();  
    	//得到用户的浏览器名  
    	String useros = "";//st.nextToken();  
    	//得到用户的操作系统名  
    	String userbrowser =""; //st.nextToken();  
    	
    	AgentClientVo agentClient=new AgentClientVo();
    	agentClient.setIp(Common.getClientIp(request));
    	agentClient.setUserbrowser(userbrowser);
    	agentClient.setUseros(useros);
    	return agentClient;
    }*/
	/**
	 * 保存文件
	 * 
	 * @param path
	 * @param file
	 * @throws Exception
	 */
	public static void saveFile(String path, File file) throws Exception {
		FileOutputStream fos = new FileOutputStream(path);
		// 建立文件上传流
		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = fis.read(buffer)) > 0) {
			fos.write(buffer, 0, len);
		}
		fis.close();
		fos.close();
	}

	public static String getDate(Date beginDate,Date endDate) {
		String result="";
		try{
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			java.util.Date now = df.parse("2015-07-13 13:31:40");
			java.util.Date date = df.parse("2015-07-13 13:33:24");
			long l = date.getTime() -now.getTime();
			long day = l / (24 * 60 * 60 * 1000);
			long hour = (l / (60 * 60 * 1000) - day * 24);
			long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
			long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
			
			if(day>0){
				result=day+"天";
			}else if(hour>0){
				result=hour+"小时";
			}else if(min>0){
				result=min+"分钟";
			}
			System.out.println(result+"---" + day + "天" + hour + "小时" + min + "分" + s + "秒");
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
 
		return result;
	}
	/** 
	* 返回文字描述的日期 
	* 
	* @param date 
	* @return 
	*/ 
	public static String getTimeFormatText(Date date) { 
		long minute = 60 * 1000;// 1分钟 
		long hour = 60 * minute;// 1小时 
	    long day = 24 * hour;// 1天 
		long month = 31 * day;// 月 
	    long year = 12 * month;// 年 
		 
	    if (date == null) { 
	      return null; 
	    } 
	    long diff = new Date().getTime() - date.getTime(); 
	    long r = 0; 
	    if (diff > year) { 
	      r = (diff / year); 
	      return r + "年前"; 
	    } 
	    if (diff > month) { 
	      r = (diff / month); 
	      return r + "个月前"; 
	    } 
	    if (diff > day) { 
	      r = (diff / day); 
	      return r + "天前"; 
	    } 
	    if (diff > hour) { 
	      r = (diff / hour); 
	      return r + "个小时前"; 
	    } 
	    if (diff > minute) { 
	      r = (diff / minute); 
	      return r + "分钟前"; 
	    } 
	    return "刚刚"; 
	}
	@SuppressWarnings("deprecation")
	public static Date getCheckDate() throws ParseException{ 
		SimpleDateFormat simp1 = new SimpleDateFormat("yyyy-MM-dd HH");

		Date serverDate =new Date(); 
		Date checkServerHour = simp1.parse(simp1.format(serverDate));

		int minHours = serverDate.getHours() + 4;
		int serverHours = serverDate.getHours();

		if (serverHours < 9) {
			minHours = 9 + 4;
		}

		if (minHours > 16 && serverHours < 17) {// 只能到第二天
			int minHours1 = minHours - 17 + 9;
			checkServerHour.setDate(checkServerHour.getDate() + 1);
			checkServerHour.setHours(minHours1);
		}
		if (minHours <= 16) {// 当前天就可以预约
			checkServerHour.setHours(minHours);
		}
		if (serverHours > 16) {
			minHours = 9 + 4;
			checkServerHour.setDate(checkServerHour.getDate() + 1);
			checkServerHour.setHours(minHours);
		} 
		return checkServerHour;
	}
	/**
     * emoji表情替换
     *
     * @param source 原字符串
     * @param slipStr emoji表情替换成的字符串                
     * @return 过滤后的字符串
     */
    public static String filterEmoji(String source,String slipStr) {
        if(StringUtils.isNotBlank(source)){
            return source.replaceAll("[\\ud800\\udc00-\\udbff\\udfff\\ud800-\\udfff]", slipStr);
        }else{
            return source;
        }
    }
}
