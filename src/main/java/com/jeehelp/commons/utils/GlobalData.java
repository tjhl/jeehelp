package com.jeehelp.commons.utils;
 
/**
 * 全局数据变量
 * 
 * @author xj date： 2017-04-18 下午11:03:19
 */
public class GlobalData {
	/**
	 * 后台用户session
	 */
	public static final String SESSION_ADMIN_USER = "adminUser";
 
	/**
	 * 网站用户session
	 */
	public static final String SESSION_WEB_USER = "webUser";
	 
    /**
	 * 统一返回成功编码 
	 */
	public static final String SUCCESS_CODE = "000000";
	/**
	 * 统一返回失败编码 
	 */
	public static final String FAILURE_CODE = "999999";
	  
	/**
	 * 字典表名字序列化器分割符
	 */
	public static final String DICT_SPLIT = "$#";
	
	/**
	 *项目类型
	 */
	public static String PROJECT_TYPES[]={"自用","客户","产品"};
	/**
	 *计划进度类型
	 */
	public static String PROJECT_PLAN_STATUS[]={"进行中","正常完成","超时完成"};
	/**
	 *项目总监
	 */
	public static final String ROLE_MANAGE = "项目总监";
//	/**
//	 *前台预约订单状态
//	 */
//	public static String FRONT_ORDERS_STATE[]={"待付款","进行中","已完成"};
 
 
}
