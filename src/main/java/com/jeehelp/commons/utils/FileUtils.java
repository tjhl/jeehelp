package com.jeehelp.commons.utils;

import java.io.File;

import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
	/**
	 * 文件写入
	 * @param uploadPath
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static File transferFile(String uploadPath, MultipartFile file) throws Exception {
		File uploadFilePath = new File(uploadPath);
		if (!uploadFilePath.exists()) {
			uploadFilePath.mkdirs();
		}  
		File finishfile = new File(uploadFilePath.getAbsolutePath() + "/" + Common.getRandom() + file.getOriginalFilename());

		file.transferTo(finishfile);

		return finishfile;
	}
	/**
	 * 文件删除
	 * @param filePath
	 * @return
	 */
	public static boolean deleteFile(String filePath) {
		File file = new File(filePath);
		if (file.isFile()) {
			file.delete();
			return true;
		}
		return false;
	}
}
