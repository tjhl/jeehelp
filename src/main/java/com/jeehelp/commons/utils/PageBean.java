package com.jeehelp.commons.utils;

import java.util.List;
/**
 * 分页工具类
 * @author xinjun
 *
 * 2017年6月17日
 */
public class PageBean<T> {

	private int count = 0; // 记录总数
 
	private int first;//从多少开始
	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	private int pageSize = 10; // 每页显示记录数

	private int pageCount = 0; // 总页数

	private int page = 1; // 当前页数

	private String totalCountSQL;// 得到总记录数sql语句

	private String listSQL;// 得到查询记录sql语句
	@SuppressWarnings("rawtypes")
	private List datalist;
	protected T t;
	
	/**
	 * 搜索关键词
	 */
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	@SuppressWarnings("rawtypes")
	public List getDatalist() {
		return datalist;
	}

	public T getT() {
		return t;
	}

	public void setT(T t) {
		this.t = t;
	}

	@SuppressWarnings("rawtypes")
	public void setDatalist(List datalist) {
		this.datalist = datalist;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		if (pageSize != 0) {
			pageCount = count / pageSize;
			if (count % pageSize != 0) {
				pageCount++;
			}

		}
		this.count = count;
	}

	public String getListSQL() {
		return listSQL;
	}

	public void setListSQL(String listSQL) {
		this.listSQL = listSQL;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getTotalCountSQL() {
		return totalCountSQL;
	}

	public void setTotalCountSQL(String totalCountSQL) {
		this.totalCountSQL = totalCountSQL;
	}
}
