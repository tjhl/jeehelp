package com.jeehelp.commons.mybatis;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.jeehelp.commons.utils.BeanUtils;

/**
 * 
* @ClassName: MyWrapper 
* @Description: 复写 mybatisplus EntityWrapper
* @author yubin
* @date 2017年9月13日 下午3:51:45 
* 
* @param <T>
 */
public class MyWrapper<T> extends EntityWrapper<T> {

	private static final long serialVersionUID = 1L;

	public MyWrapper(T entity) {
		try {
			BeanUtils.filterBeanPropertyEmptyToNull(entity);
		} catch (Exception e) {
		}
		setEntity(entity);
	}
}
