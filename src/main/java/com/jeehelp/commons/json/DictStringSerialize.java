package com.jeehelp.commons.json;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.jeehelp.commons.scan.DictUtils;
import com.jeehelp.commons.utils.GlobalData;

/**
 * 字典转换器
 * @author xinjun
 *
 * 2017年8月30日
 */
public class DictStringSerialize extends JsonSerializer<String> {
 
	
	public DictStringSerialize() {
		super();
	}
	
	public DictStringSerialize(String key) {
		super();
	}
	/**
	 * 处理转换
	 * @author xinjun
	 */
	@Override
	public void serialize(String value, JsonGenerator jgen, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		if(StringUtils.isNotBlank(value)){
			if(value.indexOf(GlobalData.DICT_SPLIT)==-1) {
				new RuntimeException("必须有分隔符！");
			}else {
				String values[]=value.split("\\"+GlobalData.DICT_SPLIT);
				  
				//根据字典值和类型关键字查询字典名称
	            String name=DictUtils.getDictName(values[0],values[1], "");
				if(name!=null){
					jgen.writeString(name);
				}
				else{
					jgen.writeString("");
				}
			}
		}
		else{
			jgen.writeString("");
		}
	}
}
