package com.jeehelp.commons.shiro;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.jeehelp.system.entity.SystemResources;
import com.jeehelp.system.entity.SystemUser;
import com.jeehelp.system.service.ISystemRoleResourcesService;
import com.jeehelp.system.service.ISystemUserService; 

/**
 * @description：shiro权限认证
 * @author：xinjun 
 * @date：2017/07/01 14:51
 */
public class ShiroDbRealm extends AuthorizingRealm {
	private static final Logger LOGGER = LogManager.getLogger(ShiroDbRealm.class);
 

	@Autowired
	private ISystemUserService iSystemUserService;
	@Autowired
	private ISystemRoleResourcesService resourcesService;
 
 
	public ShiroDbRealm(CacheManager cacheManager, CredentialsMatcher matcher) {
		super(cacheManager, matcher);
	}

	/**
	 * Shiro登录认证(原理：用户提交 用户名和密码 --- shiro 封装令牌 ---- realm 通过用户名将密码查询返回 ----
	 * shiro 自动去比较查询出密码和用户输入密码是否一致---- 进行登陆控制 )
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken)
			throws AuthenticationException {
		LOGGER.info("Shiro开始登录认证");
	  
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		 
		EntityWrapper<SystemUser> wrapper = new EntityWrapper<SystemUser>();
		wrapper.where("user_name={0}", token.getUsername());
		SystemUser user=iSystemUserService.selectOne(wrapper); 
		
		if(user==null){
			return null;
		}
		// 查询该用户按钮级别的权限列表
		SystemResources resources=new SystemResources();
		resources.setId(user.getId());
		resources.setType(1);
		
 		List<SystemResources> userResourcesList = resourcesService.selectResourcesByUserId(resources);
 		Set<String> urlSet = new HashSet<String>(); 
		for (SystemResources resource : userResourcesList) {
			if(resource.getPermission()!=null){
				urlSet.add(resource.getPermission());
			}
		}
 
		ShiroUser shiroUser = new ShiroUser(user.getId().longValue(), user.getUserName(), user.getRealName(), urlSet);
//		shiroUser.setRoleId(user.getRole().longValue());
//		shiroUser.setRoles(roles);

		// 认证缓存信息
		return new SimpleAuthenticationInfo(shiroUser, user.getUserPass().toCharArray(), getName());

	}

	/**
	 * Shiro权限认证
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		ShiroUser shiroUser = (ShiroUser) principals.getPrimaryPrincipal();

		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

		info.addStringPermissions(shiroUser.getUrlSet());

		return info;
	}

	@Override
	public void onLogout(PrincipalCollection principals) {
		super.clearCachedAuthorizationInfo(principals);
		ShiroUser shiroUser = (ShiroUser) principals.getPrimaryPrincipal();
		removeUserCache(shiroUser);
	}

	/**
	 * 清除用户缓存
	 * 
	 * @param shiroUser
	 */
	public void removeUserCache(ShiroUser shiroUser) {
		removeUserCache(shiroUser.getLoginName());
	}

	/**
	 * 清除用户缓存
	 * 
	 * @param loginName
	 */
	public void removeUserCache(String loginName) {
		SimplePrincipalCollection principals = new SimplePrincipalCollection();
		principals.add(loginName, super.getName());
		super.clearCachedAuthenticationInfo(principals);
	}
}
