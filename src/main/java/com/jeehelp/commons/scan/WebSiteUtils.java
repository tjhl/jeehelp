package com.jeehelp.commons.scan;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jeehelp.system.entity.SystemWebsite;
import com.jeehelp.system.service.ISystemWebsiteService;

/**
 * 平台网站工具类
 * 
 * @author xj 2017年7月16日
 */
@Component
public class WebSiteUtils {
	@Autowired
	private ISystemWebsiteService websiteService;
	private static WebSiteUtils webSiteUtils;
	
	@PostConstruct
	public void init() {
		webSiteUtils = this;
		webSiteUtils.websiteService = this.websiteService;
	}
	/**
	 * 获取平台网站信息
	 * @return
	 */
    public static SystemWebsite getSite(){
    	return webSiteUtils.websiteService.getWebSite();
    }
}
