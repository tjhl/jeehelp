package com.jeehelp.commons.scan;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jeehelp.system.entity.SystemDict;
import com.jeehelp.system.service.ISystemDictService;

/**
 * 字典工具类
 * 
 * @author xj 2017年7月16日
 */
@Component
public class DictUtils {
	@Autowired
	private ISystemDictService dictService;
	private static DictUtils dictUtils;

	@PostConstruct
	public void init() {
		dictUtils = this;
		dictUtils.dictService = this.dictService;
	}

	/**
	 * 根据类型查询所有的字典数据
	 * 
	 * @param type
	 * @return
	 */
	public static List<SystemDict> getDictList(String type) {
		// 先查询所有的字典数据然后内存里面筛选出要查询类型的字典数据
		List<SystemDict> dictList = dictUtils.dictService.getDictList();
		List<SystemDict> typeDictList = new ArrayList<SystemDict>();
		if (dictList != null) {
			for (SystemDict systemDict : dictList) {
				if (systemDict.getTypeKey() != null && systemDict.getTypeKey().equals(type)) {
					typeDictList.add(systemDict);
				}
			}
		}
		return typeDictList;
	}
    /**
     * 根据字典值和类型关键字查询字典名称
     * @param value
     * @param type
     * @param defaultValue
     * @return
     */
	public static String getDictName(String value, String type, String defaultValue) {

		List<SystemDict> dictList = dictUtils.dictService.getDictList();
		if (dictList != null) {
			for (SystemDict systemDict : dictList) {
				if (systemDict.getTypeKey() != null && systemDict.getTypeKey().equals(type)&&systemDict.getValue().equals(value)) {
					return systemDict.getName(); 
				}
			}
		}
		return defaultValue;
	}
	 /**
     * 根据字典名称和类型关键字查询字典值 
     * @param value
     * @param type
     * @param defaultValue
     * @return
     */
	public static String getDictValue(String name, String type, String defaultName) {
		List<SystemDict> dictList = dictUtils.dictService.getDictList();
		if (dictList != null) {
			for (SystemDict systemDict : dictList) {
				if (systemDict.getTypeKey() != null && systemDict.getTypeKey().equals(type)&&systemDict.getName().equals(name)) {
					return systemDict.getName(); 
				}
			}
		}
		return defaultName;
	}
}
