package com.jeehelp.commons.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jeehelp.commons.utils.FileUtils;
import com.jeehelp.commons.utils.UploaderUtil;
import com.jeehelp.commons.utils.UploaderUtil.UploadResult;

/**
 * 公用上传文件controller
 * 
 * @author xinjun
 *
 *         2017年7月25日
 */

@Controller
@RequestMapping("/common/file")
public class FileUploadController {
	/**
	 * 上传路径
	 */
	@Value("${upload.basedir}")
	public String basedir;
	 

	@RequestMapping("/upLoad")
	@ResponseBody
	public Object  upLoad(@RequestParam("file") MultipartFile file) {
		File diskFile;
		try {
			String folder="fileupload";
			diskFile = FileUtils.transferFile(basedir, file);
			String realPath = "{\"path\":\"" + folder + "/" + diskFile.getName() + "\"}";
			return realPath;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/webUpload", method = RequestMethod.POST)
	public ModelAndView upload(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView m = new ModelAndView();
		UploadResult result = UploaderUtil.uploader(request, "/data", "filenameUploader", "/uploader", null,
				1024 * 1024);
		m.addObject("result", result);
		return m;
	}
}
