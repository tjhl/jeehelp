package com.jeehelp.wechat.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 微信公众平台用户
 * </p>
 *
 * @author xj
 * @since 2017-09-10
 */
@TableName("wechat_user")
public class WechatUser extends Model<WechatUser> {

    private static final long serialVersionUID = 1L;

	private Long id;
    /**
     * 是否关注 1关注 0取消关注
     */
	private Integer subscribe;
	private String openid;
    /**
     * 昵称
     */
	private String nickname;
	/**
	 * 性别
	 */
	private String sex;
    /**
     * 城市
     */
	private String city;
    /**
     * 国家
     */
	private String country;
    /**
     * 省份
     */
	private String province;
    /**
     * 语言
     */
	private String language;
    /**
     *  头像图片
     */
	private String headimgurl;
    /**
     * 关注时间
     */
	@TableField("subscribe_time")
	private String subscribeTime;
    /**
     * 取消关注时间
     */
	@TableField("unsubscribe_time")
	private String unsubscribeTime;
	private String remark;
	@TableField("create_time")
	private Date createTime;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(Integer subscribe) {
		this.subscribe = subscribe;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	public String getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(String subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

	public String getUnsubscribeTime() {
		return unsubscribeTime;
	}

	public void setUnsubscribeTime(String unsubscribeTime) {
		this.unsubscribeTime = unsubscribeTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
