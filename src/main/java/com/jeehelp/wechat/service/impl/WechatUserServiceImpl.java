package com.jeehelp.wechat.service.impl;


import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.wechat.entity.WechatUser;
import com.jeehelp.wechat.dao.WechatUserMapper;
import com.jeehelp.wechat.service.IWechatUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

/**
 * <p>
 * 微信公众平台用户 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-09-10
 */
@Service
public class WechatUserServiceImpl extends ServiceImpl<WechatUserMapper, WechatUser> implements IWechatUserService {
	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo,WechatUser wechatUser) {
		Page<WechatUser> page = new Page<WechatUser>(pageInfo.getPage(), pageInfo.getPageSize());

		EntityWrapper<WechatUser> wrapper = new EntityWrapper<WechatUser>(wechatUser);
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));
		 
		selectPage(page, wrapper);

		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}
}
