package com.jeehelp.wechat.service;

import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.wechat.entity.WechatUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 微信公众平台用户 服务类
 * </p>
 *
 * @author xj
 * @since 2017-09-10
 */
public interface IWechatUserService extends IService<WechatUser> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo,WechatUser wechatUser);
}
