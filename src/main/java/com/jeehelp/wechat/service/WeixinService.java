package com.jeehelp.wechat.service;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.binarywang.wxpay.bean.request.WxPayRefundRequest;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.jeehelp.wechat.config.WeChatConfig;
import com.jeehelp.wechat.handler.LogHandler;
import com.jeehelp.wechat.handler.MenuHandler;
import com.jeehelp.wechat.handler.MsgHandler;
import com.jeehelp.wechat.handler.SubscribeHandler;
import com.jeehelp.wechat.handler.UnsubscribeHandler;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.kefu.result.WxMpKfOnlineList;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

/**
 * 微信接口
 * Created by houyi on 2017/09/01.
 */
@Service
public class WeixinService extends WxMpServiceImpl {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WeChatConfig weChatConfig;

    @Autowired
    protected LogHandler logHandler;

    @Autowired
    private MenuHandler menuHandler;

    @Autowired
    private MsgHandler msgHandler;

    @Autowired
    private UnsubscribeHandler unsubscribeHandler;

    @Autowired
    private SubscribeHandler subscribeHandler;

//    private WxPayService wxService=new WxPayServiceImpl();

    private WxMpMessageRouter router;

    @PostConstruct
    public void init() {
        final WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage();
        config.setAppId(this.weChatConfig.getAppid());// 设置微信公众号的appid
        config.setSecret(this.weChatConfig.getAppsecret());// 设置微信公众号的app corpSecret
        config.setToken(this.weChatConfig.getToken());// 设置微信公众号的token
        config.setAesKey(this.weChatConfig.getAesKey());// 设置消息加解密密钥
        super.setWxMpConfigStorage(config);

        this.refreshRouter();
    }

    /**
     * 刷新路由
     */
    private void refreshRouter() {
        final WxMpMessageRouter newRouter = new WxMpMessageRouter(this);

        // 记录所有事件的日志
        newRouter.rule().handler(this.logHandler).next();

        // 自定义菜单事件
        newRouter.rule().async(false).msgType(WxConsts.XML_MSG_EVENT)
                .event(WxConsts.BUTTON_CLICK).handler(this.menuHandler).end();

        // 关注事件
        newRouter.rule().async(false).msgType(WxConsts.XML_MSG_EVENT)
                .event(WxConsts.EVT_SUBSCRIBE).handler(this.subscribeHandler).end();

        // 取消关注事件
        newRouter.rule().async(false).msgType(WxConsts.XML_MSG_EVENT)
                .event(WxConsts.EVT_UNSUBSCRIBE).handler(this.unsubscribeHandler).end();

        // 默认
        newRouter.rule().async(false).handler(this.msgHandler).end();

        this.router = newRouter;
    }

    /**
     * 路由消息
     * @param message
     * @return
     */
    public WxMpXmlOutMessage route(WxMpXmlMessage message) {
        try {
            return this.router.route(message);
        } catch (Exception e) {
            this.logger.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * 判断是否有在线客服
     * @return
     */
    public boolean hasKefuOnline() {
        try {
            WxMpKfOnlineList kfOnlineList = this.getKefuService().kfOnlineList();
            return kfOnlineList != null && kfOnlineList.getKfOnlineList().size() > 0;
        } catch (Exception e) {
            this.logger.error("获取客服在线状态异常: " + e.getMessage(), e);
        }

        return false;
    }

    /**
     * 全额退款
     * @param outRefundNo 商户退款单号
     * @param outTradeNo 商户订单号
     * @throws WxPayException
     */
    public void refoundAll(String outRefundNo, String outTradeNo, BigDecimal payFee) throws WxPayException {
        WxPayRefundRequest request = WxPayRefundRequest.newBuilder()
                .outRefundNo(outRefundNo)
                .outTradeNo(outTradeNo)
                .totalFee(payFee.multiply(new BigDecimal(100)).intValue())
                .refundFee(payFee.multiply(new BigDecimal(100)).intValue())
                .build();
//        WxPayRefundResult result = payService.refund(request);
//        this.logger.info(result.toString());
    }


}
