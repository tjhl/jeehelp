package com.jeehelp.wechat.controller;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jeehelp.wechat.service.WeixinService;

import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

/**
 * 微信oauth2验证跳转页面
 * Created by houyi on 2017/4/6.
 */
@Controller
public class OAuth2Controller {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WeixinService weixinService;

    /**
     * URL跳转页面
     *
     * @param request
     * @param response
     * @param code
     * @param oauth2url
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/wechat/oauth2RedirectURL")
    public String oauth2RedirectURL(HttpServletRequest request, HttpServletResponse response, @RequestParam String code, @RequestParam String oauth2url) throws IOException, WxErrorException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        // 用户同意授权
        if (!"authdeny".equals(code)) {
            // 网页授权接口访问凭证
            WxMpOAuth2AccessToken wxMpOAuth2AccessToken = weixinService.oauth2getAccessToken(code);
            boolean valid = weixinService.oauth2validateAccessToken(wxMpOAuth2AccessToken);
            if (!valid) {
                wxMpOAuth2AccessToken = weixinService.oauth2refreshAccessToken(wxMpOAuth2AccessToken.getRefreshToken());
            }

            WxMpUser wxMpUser = weixinService.oauth2getUserInfo(wxMpOAuth2AccessToken, null); // snsapi_userinfo 认证方式获取用户完整信息
            
            logger.info("打印网页授权："+wxMpUser);
            if(wxMpUser!=null){
            	request.getSession().setAttribute("wxUser", wxMpUser);
            }
            //获取微信用户openid存储在cookie中的信息
            Cookie userCookie = new Cookie("vein-openId", wxMpOAuth2AccessToken.getOpenId());
            userCookie.setMaxAge(-1);
            userCookie.setPath("/");
            response.addCookie(userCookie);
        }
        return "redirect:" + URLDecoder.decode(oauth2url, "utf-8");
    }

}
