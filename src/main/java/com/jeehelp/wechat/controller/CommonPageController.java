package com.jeehelp.wechat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * wap公用页面处理
 * @author xj
 * 2017年9月10日
 */
@Controller
@RequestMapping("/wechat")
public class CommonPageController {
 
	/**
	 * 必须微信里面打开错误提示页面
	 * @param model
	 * @return
	 */
	@RequestMapping("/error")
	public String error(Model model) {
		return "/wap/errorpage/oauth";
	}

}
