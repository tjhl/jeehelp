package com.jeehelp.wechat.handler;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.jeehelp.wechat.entity.WechatUser;
import com.jeehelp.wechat.service.IWechatUserService;

import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

/**
 * 微信取消关注处理
 * Created by houyi on 2017/09/01.
 */
@Component
public class UnsubscribeHandler extends AbstractHandler {
	@Autowired
    private IWechatUserService weChatUserService;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService wxMpService,
                                    WxSessionManager sessionManager) {
        String openId = wxMessage.getFromUser();
        this.logger.info("取消关注用户 OPENID: " + openId);

        WechatUser user = new WechatUser();
        user.setOpenid(openId);
        EntityWrapper<WechatUser> wrapper = new EntityWrapper<WechatUser>(user);
        user=weChatUserService.selectOne(wrapper);
        
        if(user!=null){
        	user.setSubscribe(0);
        	user.setUnsubscribeTime(String.valueOf(wxMessage.getCreateTime()));
        	
        	weChatUserService.updateById(user);
        }
        return null;
    }

}
