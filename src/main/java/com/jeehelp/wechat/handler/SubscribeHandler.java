package com.jeehelp.wechat.handler;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jeehelp.wechat.builder.TextBuilder;
import com.jeehelp.wechat.entity.WechatUser;
import com.jeehelp.wechat.service.IWechatUserService;
import com.jeehelp.wechat.service.WeixinService;

import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

/**
 * 微信关注处理
 *
 * Created by houyi on 2017/09/01.
 */
@Component
public class SubscribeHandler extends AbstractHandler {

    @Autowired
    private IWechatUserService weChatUserService;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService,
                                    WxSessionManager sessionManager) throws WxErrorException {

        this.logger.info("新关注用户 OPENID: " + wxMessage.getFromUser());

        WeixinService weixinService = (WeixinService) wxMpService;

        // 获取微信用户基本信息
        WxMpUser userWxInfo = weixinService.getUserService().userInfo(wxMessage.getFromUser(), null);

        if (userWxInfo != null) {
            WechatUser user = new WechatUser();
            user.setOpenid(userWxInfo.getOpenId());
            user.setNickname(userWxInfo.getNickname());
            user.setSex(userWxInfo.getSex());
            user.setLanguage(userWxInfo.getLanguage());
            user.setCity(userWxInfo.getCity());
            user.setProvince(userWxInfo.getProvince());
            user.setCountry(userWxInfo.getCountry());
            user.setHeadimgurl(userWxInfo.getHeadImgUrl());
            user.setSubscribe(userWxInfo.getSubscribe()?1:0);
            user.setSubscribeTime(String.valueOf(userWxInfo.getSubscribeTime()));
            user.setCreateTime(new Date());
            weChatUserService.insert(user);
        }

        try {
            return new TextBuilder().build("感谢关注xx微信公众号", wxMessage, weixinService);
        } catch (Exception e) {
            this.logger.error(e.getMessage(), e);
        }

        return null;
    }


}
