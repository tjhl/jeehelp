package com.jeehelp.wechat.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 微信配置
 * Created by houyi on 2017/6/8.
 */
@Configuration
public class WeChatConfig {
    @Value("${wx.token}")
    private String token;

    @Value("${wx.appId}")
    private String appid;

    @Value("${wx.appsecret}")
    private String appsecret;

    @Value("${wx.aeskey}")
    private String aesKey;

    @Value("${wx.server.name}")
    private String serverName;

    @Value("${wx.oauth2.redirect.url}")
    private String oauth2RedirectURL;

    @Value("${wx.mchId}")
    public String mchId;

    @Value("${wx.partnerKey}")
    public String partnerKey;

    public String getServerName() {
        return serverName;
    }

    public String getOauth2RedirectURL() {
        return oauth2RedirectURL;
    }

    public String getToken() {
        return token;
    }

    public String getAppid() {
        return appid;
    }

    public String getAppsecret() {
        return appsecret;
    }

    public String getAesKey() {
        return aesKey;
    }

    public String getMchId() {
        return mchId;
    }

    public String getPartnerKey() {
        return partnerKey;
    }
}
