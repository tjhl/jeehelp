package com.jeehelp.wechat.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jeehelp.wechat.config.WeChatConfig;
import com.jeehelp.wechat.service.WeixinService;

import me.chanjar.weixin.common.api.WxConsts;

/**
 * URL拦截器
 * Created by houyi on 2017/4/6.
 */
public class OAuth2Interceptor extends HandlerInterceptorAdapter {

    @Autowired
    private WeixinService wxService;

    @Autowired
    private WeChatConfig weChatConfig;

    /**
     * 过滤请求地址
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(isWeChat(request)){
            String openId = findCookieByName(request, "vein-openId");
            if (handler instanceof HandlerMethod) {
                if (StringUtils.isEmpty(openId)) {
                    String url = request.getScheme() + "://" + weChatConfig.getServerName() + weChatConfig.getOauth2RedirectURL() + "?oauth2url=" + request.getRequestURI();
                    String oauth2Url = wxService.oauth2buildAuthorizationUrl(url, WxConsts.OAUTH2_SCOPE_USER_INFO, null);
                    response.sendRedirect(oauth2Url);
                    return false;
                }
            }else {
                return true;
            }
        }else {
            String errorUrl = request.getScheme() + "://" + weChatConfig.getServerName() + "/wechat/error";
            response.sendRedirect(errorUrl);
            return false;
        }
        return true;
    }

    /**
     * 查询cookie
     *
     * @param request
     * @param name
     * @return
     */
    private String findCookieByName(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        Cookie result = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    result = cookie;
                    break;
                }
            }
        }
        return result == null ? null : result.getValue();
    }

    /**
     * 判断是否微信访问
     * @param request
     * @return
     */
    private boolean isWeChat(HttpServletRequest request) {
        String userAgent = request.getHeader("user-agent").toLowerCase();
        return userAgent == null || userAgent.indexOf("micromessenger") == -1 ? false : true;
    }

}
