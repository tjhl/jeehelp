package com.jeehelp.wechat.dao;

import com.jeehelp.wechat.entity.WechatUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 微信公众平台用户 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-09-10
 */
public interface WechatUserMapper extends BaseMapper<WechatUser> {

}