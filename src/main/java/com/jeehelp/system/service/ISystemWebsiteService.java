package com.jeehelp.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemWebsite;

/**
 * <p>
 * 网站信息 服务类
 * </p>
 *
 * @author xj
 * @since 2017-07-16
 */
public interface ISystemWebsiteService extends IService<SystemWebsite> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo);
	 
	 SystemWebsite getWebSite();
	 boolean update(SystemWebsite obj);
}
