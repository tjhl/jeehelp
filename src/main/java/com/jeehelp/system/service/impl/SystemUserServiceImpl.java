package com.jeehelp.system.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.commons.shiro.ShiroUser;
import com.jeehelp.commons.utils.MD5Util;
import com.jeehelp.system.dao.SystemUserMapper;
import com.jeehelp.system.dao.SystemUserRoleMapper;
import com.jeehelp.system.entity.SystemUser;
import com.jeehelp.system.entity.SystemUserRole;
import com.jeehelp.system.service.ISystemUserService;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
@Service
public class SystemUserServiceImpl extends ServiceImpl<SystemUserMapper, SystemUser> implements ISystemUserService {

	@Resource
	private SystemUserRoleMapper systemUserRoleMapper;

	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo, SystemUser systemUser) {
		Map<String, Object> condition = new HashMap<String, Object>();
		pageInfo.setSort("id");
		pageInfo.setOrder("desc");

		if (!StringUtils.isEmpty(pageInfo.getKey())) {
			condition.put("key", pageInfo.getKey());
		}
		pageInfo.setCondition(condition);
		
		Page<SystemUser> page = new Page<SystemUser>(pageInfo.getPage(), pageInfo.getPageSize());

		systemUser.setIsdel(0);
		EntityWrapper<SystemUser> wrapper = new EntityWrapper<SystemUser>(systemUser);
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));

		if (!StringUtils.isEmpty(pageInfo.getKey())) {
			wrapper.like("user_name", pageInfo.getKey()).or();
			wrapper.like("real_name", pageInfo.getKey());
		}

		selectPage(page, wrapper);
		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}

	/**
	 * 添加用户
	 * 
	 * @param obj
	 * @return
	 */
	@Override
	public boolean insertServer(SystemUser obj) {
		// TODO Auto-generated method stub
		obj.setCreateDate(new Date());
		obj.setUserPass(MD5Util.string2MD5(obj.getUserPass()));
		
		if (super.insert(obj)) {
			if (!StringUtils.isEmpty(obj.getRoleIds())) {
				String[] strids = obj.getRoleIds().split(",");
				for (String id : strids) {
					SystemUserRole userRole = new SystemUserRole();
					userRole.setUserId(obj.getId());
					userRole.setRoleId(Long.parseLong(id));
					userRole.setCreateDate(new Date());
					userRole.setCreateUser(obj.getCreateUser());

					systemUserRoleMapper.insert(userRole);
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * 修改用户
	 * 
	 * @param obj
	 * @return
	 */
	@Override
	public boolean updateServer(SystemUser obj) {
		// TODO Auto-generated method stub
		if (super.updateById(obj)) {
			//先删除掉该用户的角色
			SystemUserRole delUserRole=new SystemUserRole();
			delUserRole.setUserId(obj.getId());
			EntityWrapper<SystemUserRole> wrapper = new EntityWrapper<SystemUserRole>(delUserRole);
			systemUserRoleMapper.delete(wrapper);
			
			if (!StringUtils.isEmpty(obj.getRoleIds())) {
				String[] strids = obj.getRoleIds().split(",");
				for (String id : strids) {
					SystemUserRole userRole = new SystemUserRole();
					userRole.setUserId(obj.getId());
					userRole.setRoleId(Long.parseLong(id));
					userRole.setCreateDate(new Date());
					userRole.setCreateUser(obj.getCreateUser());

					systemUserRoleMapper.insert(userRole);
				}
			}
			return true;
		}
		return false;
	}
	 /**
	  * 系统登录
	  * @author xinjun
	  * @param loginuser
	  * @return
	  */
	@Override
	public Object loginServer(SystemUser loginuser) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		try { 
			Subject user = SecurityUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(loginuser.getUserName(),
					 loginuser.getUserPass());
			token.setRememberMe(true);
			user.login(token); 
			map.put("result", true);
		} catch (UnknownAccountException e) {
			//e.printStackTrace();
			map.put("result", false); 
			map.put("msg", "账号不存在");
		} catch (DisabledAccountException e) {
//			e.printStackTrace();
			map.put("result", false);
			map.put("msg", "账号未启用");
		} catch (IncorrectCredentialsException e) {
//			e.printStackTrace();
			map.put("result", false);
			map.put("msg", "密码错误");
		} catch (RuntimeException e) {
//			e.printStackTrace();
			map.put("result", false);
			map.put("msg", "未知错误,请联系管理员");
		} 
		return map;
	}
	/**
	  * 密码修改
	  * @param oldPwd
	  * @param userPass
	  * @return
	*/
	@Override
	public Object updatePwdServer(String oldPwd, String userPass) {
		// TODO Auto-generated method stub
		HashMap<String,Object> resultMap=new HashMap<String,Object>(); 
		ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		
		SystemUser user= this.selectById(shiroUser.getId());
	    String md5pass=MD5Util.string2MD5(oldPwd);
		if (md5pass.equals(user.getUserPass())) { 
			user.setUserPass(MD5Util.string2MD5(userPass));
			this.updateById(user);
			
			resultMap.put("success", true);
			resultMap.put("msg", "修改成功！");
		} else {
			resultMap.put("success", false);
			resultMap.put("msg", "原密码错误！");
		} 
		return resultMap;  
	}
}
