package com.jeehelp.system.service.impl;


import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.dao.SystemWebsiteMapper;
import com.jeehelp.system.entity.SystemWebsite;
import com.jeehelp.system.service.ISystemWebsiteService;

/**
 * <p>
 * 网站信息 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-07-16
 */
@Service
public class SystemWebsiteServiceImpl extends ServiceImpl<SystemWebsiteMapper, SystemWebsite> implements ISystemWebsiteService {
	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		Page<SystemWebsite> page = new Page<SystemWebsite>(pageInfo.getPage(), pageInfo.getPageSize());

		EntityWrapper<SystemWebsite> wrapper = new EntityWrapper<SystemWebsite>();
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));
		 
		selectPage(page, wrapper);

		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}
	
	@Cacheable(value = "website-activeSessionCache")
	@Override
	public SystemWebsite getWebSite() {
		// TODO Auto-generated method stub
		return super.selectList(null).get(0);
	}
	@CacheEvict(value = "website-activeSessionCache", allEntries = true)
	@Override
	public boolean update(SystemWebsite obj) {
		// TODO Auto-generated method stub
		return super.updateById(obj);
	}
}
