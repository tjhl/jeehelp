package com.jeehelp.system.service.impl;


import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.dao.SystemDictMapper;
import com.jeehelp.system.entity.SystemDict;
import com.jeehelp.system.service.ISystemDictService;

/**
 * <p>
 * 字典 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-06-26
 */
@Service("SystemDictServiceImpl")
public class SystemDictServiceImpl extends ServiceImpl<SystemDictMapper, SystemDict> implements ISystemDictService {
	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		Page<SystemDict> page = new Page<SystemDict>(pageInfo.getPage(), pageInfo.getPageSize());

		EntityWrapper<SystemDict> wrapper = new EntityWrapper<SystemDict>();
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));
		 
		if(!StringUtils.isEmpty(pageInfo.getKey())){
			wrapper.like("type_key", pageInfo.getKey()).or();
			wrapper.like("name", pageInfo.getKey());
		}
		selectPage(page, wrapper);

		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}
	@CacheEvict(value = "dict-activeSessionCache",allEntries = true)
	@Override
	public boolean insert(SystemDict entity) {
		// TODO Auto-generated method stub
		return super.insert(entity);
	}
	@CacheEvict(value = "dict-activeSessionCache", allEntries = true)
	@Override
	public boolean update(SystemDict dict) {
		// TODO Auto-generated method stub
		return super.updateById(dict);
	}
	
	@CacheEvict(value = "dict-activeSessionCache", allEntries = true)
	@Override
	public boolean deleteIds(String ids) {
		// TODO Auto-generated method stub
		return deleteBatchIds(java.util.Arrays.asList(ids.split(",")));
	}
	@Cacheable(value = "dict-activeSessionCache")
	@Override
	public List<SystemDict> getDictList() {
		// TODO Auto-generated method stub 
		EntityWrapper<SystemDict> wrapper = new EntityWrapper<SystemDict>();
		return super.selectList(wrapper);
	}
	
}
