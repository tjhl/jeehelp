package com.jeehelp.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.dao.SystemRoleMapper;
import com.jeehelp.system.entity.SystemResources;
import com.jeehelp.system.entity.SystemRole;
import com.jeehelp.system.service.ISystemResourcesService;
import com.jeehelp.system.service.ISystemRoleResourcesService;
import com.jeehelp.system.service.ISystemRoleService;

/**
 * <p>
 * 系统角色 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
@Service
public class SystemRoleServiceImpl extends ServiceImpl<SystemRoleMapper, SystemRole> implements ISystemRoleService {

	@Autowired
	private ISystemResourcesService systemResourcesService;
	@Autowired
	private ISystemRoleResourcesService roleResourcesService;

	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		Page<SystemRole> page = new Page<SystemRole>(pageInfo.getPage(), pageInfo.getPageSize());

		EntityWrapper<SystemRole> wrapper = new EntityWrapper<SystemRole>();
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));

		selectPage(page, wrapper);

		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}

	/**
	 * 查询用户拥有的所有的角色
	 * 
	 * @param userId
	 * @return
	 */
	@Override
	public List<SystemRole> selectRoleByUserId(Long userId) {
		// TODO Auto-generated method stub
		return baseMapper.selectRoleByUserId(userId);
	}

	/**
	 * 查询角色权限
	 * 
	 * @author xinjun
	 * @param id
	 * @return
	 */
	@Override
	public Object getRoleResource(Long id) {
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		// 查询系统中所有的权限
		List<Map<String, Object>> resourceList = systemResourcesService.selectMaps(null);
		// 查询某个角色拥有的权限
		SystemResources resources = new SystemResources();
		resources.setId(id);
		List<SystemResources> roleResList = roleResourcesService.selectResourcesByT(resources);

		if (resourceList != null) {
			for (Map<String, Object> hashMap : resourceList) {
				hashMap.put("pId", hashMap.get("pid"));
				hashMap.put("open", true);
				for (SystemResources myResourceItem : roleResList) {
					if (hashMap.get("id").toString().equals(myResourceItem.getId().toString())) {
						hashMap.put("checked", true);
						break;
					}
				}
				hashMap.put("url", "");
			}
		}
		resultMap.put("success", true);
		resultMap.put("data", resourceList);
		return resultMap;
	}

	/**
	 * 查询角色列表数据
	 * 
	 * @author xinjun
	 * @return
	 */
	@Override
	public List<SystemRole> selectRoleAll() {
		// TODO Auto-generated method stub
		// 查询系统角色列表数据
		SystemRole role = new SystemRole();
		role.setIsdel(0);
		EntityWrapper<SystemRole> wrapper = new EntityWrapper<SystemRole>(role);
		return selectList(wrapper);
	}
}
