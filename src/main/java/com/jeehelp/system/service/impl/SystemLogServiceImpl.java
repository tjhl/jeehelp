package com.jeehelp.system.service.impl;


import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.dao.SystemLogMapper;
import com.jeehelp.system.entity.SystemLog;
import com.jeehelp.system.service.ISystemLogService;

/**
 * <p>
 * 后台系统日志记录 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
@Service
public class SystemLogServiceImpl extends ServiceImpl<SystemLogMapper, SystemLog> implements ISystemLogService {
	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		Page<SystemLog> page = new Page<SystemLog>(pageInfo.getPage(), pageInfo.getPageSize());

		EntityWrapper<SystemLog> wrapper = new EntityWrapper<SystemLog>();
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));
		 
		selectPage(page, wrapper);

		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}
}
