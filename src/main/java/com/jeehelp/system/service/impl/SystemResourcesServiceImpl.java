package com.jeehelp.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.dao.SystemResourcesMapper;
import com.jeehelp.system.entity.SystemResources;
import com.jeehelp.system.service.ISystemResourcesService;

/**
 * <p>
 * 系统菜单 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
@Service
public class SystemResourcesServiceImpl extends ServiceImpl<SystemResourcesMapper, SystemResources>
		implements ISystemResourcesService {
	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		Page<SystemResources> page = new Page<SystemResources>(pageInfo.getPage(), pageInfo.getPageSize());

		EntityWrapper<SystemResources> wrapper = new EntityWrapper<SystemResources>();
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));

		selectPage(page, wrapper);

		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}

	/**
	 * 查询系统中所有的权限
	 * 
	 * @return
	 */
	@Override
	public Map<String, Object> getAllRoleResource() {
		// TODO Auto-generated method stub
		HashMap<String, Object> resultMap = new HashMap<String, Object>();

		// 查询系统中所有的权限
		List<Map<String, Object>> resourceList = this.selectMaps(null);
		if (resourceList != null) {
			for (Map<String, Object> hashMap : resourceList) {
				hashMap.put("pId", hashMap.get("pid"));
				hashMap.put("open", true);
			}
		}
		resultMap.put("success", true);
		resultMap.put("data", resourceList);
		return resultMap;
	}

	/**
	 * 删除资源
	 * 
	 * @author xinjun
	 * @return
	 */
	@Override
	public Map<String, Object> deletResource(String id) {
		// TODO Auto-generated method stub
		HashMap<String, Object> resultMap = new HashMap<String, Object>();

		SystemResources res = new SystemResources();
		res.setPid(Long.parseLong(id));
		EntityWrapper<SystemResources> wrapper = new EntityWrapper<SystemResources>(res);
		int count = this.selectList(wrapper).size();
		if (count > 0) {
			resultMap.put("success", false);
		} else {
			resultMap.put("success", true);
			this.deleteById(Long.parseLong(id));
			resultMap.put("msg", "删除成功！");
		}
		return resultMap;
	}
}
