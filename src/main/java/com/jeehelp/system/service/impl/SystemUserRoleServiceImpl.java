package com.jeehelp.system.service.impl;


import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.dao.SystemUserRoleMapper;
import com.jeehelp.system.entity.SystemUserRole;
import com.jeehelp.system.service.ISystemUserRoleService;

/**
 * <p>
 * 用户角色关系表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-07-23
 */
@Service
public class SystemUserRoleServiceImpl extends ServiceImpl<SystemUserRoleMapper, SystemUserRole> implements ISystemUserRoleService {
	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		Page<SystemUserRole> page = new Page<SystemUserRole>(pageInfo.getPage(), pageInfo.getPageSize());

		EntityWrapper<SystemUserRole> wrapper = new EntityWrapper<SystemUserRole>();
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));
		 
		selectPage(page, wrapper);

		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}
}
