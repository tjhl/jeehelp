package com.jeehelp.system.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.dao.SystemRoleResourcesMapper;
import com.jeehelp.system.entity.SystemResources;
import com.jeehelp.system.entity.SystemRoleResources;
import com.jeehelp.system.service.ISystemRoleResourcesService;

/**
 * <p>
 * 系统权限中间表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
@Service
public class SystemRoleResourcesServiceImpl extends ServiceImpl<SystemRoleResourcesMapper, SystemRoleResources>
		implements ISystemRoleResourcesService {
	/**
	 * 分页查询
	 * 
	 * @param pageInfo
	 */
	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		pageInfo.setSort("id");
        pageInfo.setOrder("desc");
		Page<SystemRoleResources> page = new Page<SystemRoleResources>(pageInfo.getPage(), pageInfo.getPageSize());

		EntityWrapper<SystemRoleResources> wrapper = new EntityWrapper<SystemRoleResources>();
		wrapper.orderBy(pageInfo.getSort(), pageInfo.getOrder().equalsIgnoreCase("ASC"));

		selectPage(page, wrapper);

		pageInfo.setRows(page.getRecords());
		pageInfo.setTotal(page.getTotal());
	}

	@Override
	public List<SystemResources> selectResourcesByT(SystemResources obj) {
		// TODO Auto-generated method stub
		return baseMapper.selectResourcesByT(obj);
	}
	@Override
	public List<SystemResources> selectResourcesByUserId(SystemResources obj) {
		// TODO Auto-generated method stub
		return baseMapper.selectResourcesByUserId(obj);
	}
	@Override
	public int updateRoleResource(String ids, Long id) {
		// TODO Auto-generated method stub
		String []tempIds=ids.split(",");
		if(tempIds.length>0){
			//先删除角色下的权限
			SystemRoleResources delRoleResour=new SystemRoleResources();
			delRoleResour.setRoleId(id);
			EntityWrapper<SystemRoleResources> wrapper = new EntityWrapper<SystemRoleResources>(delRoleResour);
			baseMapper.delete(wrapper);
			
			for (String tempId : tempIds) {
				SystemRoleResources roleResour=new SystemRoleResources();
				roleResour.setResourcesId(Long.parseLong(tempId));
				roleResour.setRoleId(id);
				baseMapper.insert(roleResour);
			}
		}
		return 1;
	}
}
