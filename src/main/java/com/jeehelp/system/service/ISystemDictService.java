package com.jeehelp.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemDict;

/**
 * <p>
 * 字典 服务类
 * </p>
 *
 * @author xj
 * @since 2017-06-26
 */
public interface ISystemDictService extends IService<SystemDict> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo);
	 
	 boolean insert(SystemDict obj);
	 
	 boolean update(SystemDict obj);
	 
	 boolean deleteIds(String ids);
	 
	 /**
	  * 查询所有的字典数据
	  * @return
	  */
	 public List<SystemDict>getDictList();
	 
}
