package com.jeehelp.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemUserRole;

/**
 * <p>
 * 用户角色关系表 服务类
 * </p>
 *
 * @author xj
 * @since 2017-07-23
 */
public interface ISystemUserRoleService extends IService<SystemUserRole> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo);
}
