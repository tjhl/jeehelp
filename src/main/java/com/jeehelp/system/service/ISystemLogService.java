package com.jeehelp.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemLog;

/**
 * <p>
 * 后台系统日志记录 服务类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface ISystemLogService extends IService<SystemLog> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo);
}
