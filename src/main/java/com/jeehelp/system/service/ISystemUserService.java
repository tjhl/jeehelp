package com.jeehelp.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemUser;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface ISystemUserService extends IService<SystemUser> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo,SystemUser systemUser);
	 /**
	  * 添加用户
	  * @param obj
	  * @return
	  */
	 boolean insertServer(SystemUser obj);
	 /**
	  * 修改用户
	  * @param obj
	  * @return
	  */
	 boolean updateServer(SystemUser obj);
	 /**
	  * 密码修改
	  * @param oldPwd
	  * @param userPass
	  * @return
	  */
	 Object updatePwdServer(String oldPwd,String userPass);
	 /**
	  * 系统登录
	  * @author xinjun
	  * @param loginuser
	  * @return
	  */
	 Object loginServer(SystemUser loginuser);
}
