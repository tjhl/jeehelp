package com.jeehelp.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemResources;
import com.jeehelp.system.entity.SystemRoleResources;

/**
 * <p>
 * 系统权限中间表 服务类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface ISystemRoleResourcesService extends IService<SystemRoleResources> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo);
	 
	 /**
	  * 根据用户 id查询资源信息
	  * @param param
	  * @return
	  */
	 List<SystemResources> selectResourcesByUserId(SystemResources obj);
	 
	 /**
	  * 根据角色id查询资源信息
	  * @param param
	  * @return
	  */
	 List<SystemResources> selectResourcesByT(SystemResources obj);
	 
	
	 /**
	  * 更新用户权限
	  * @param ids
	  * @param id
	  * @return
	  */
	 int updateRoleResource(String ids,Long id);
}
