package com.jeehelp.system.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemRole;

/**
 * <p>
 * 系统角色 服务类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface ISystemRoleService extends IService<SystemRole> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo);
	 
	 /**
	  * 查询用户拥有的所有的角色
	  * @param userId
	  * @return
	  */
	 List<SystemRole> selectRoleByUserId(Long userId);
	 
	 /**
	  * 查询角色权限
	  * @author xinjun
	  * @param id
	  * @return
	  */
	 Object getRoleResource(Long id);
	 
	 /**
	  * 查询角色列表数据
	  * @author xinjun
	  * @return
	  */
	 List<SystemRole> selectRoleAll();
}
