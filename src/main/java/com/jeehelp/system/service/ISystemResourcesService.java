package com.jeehelp.system.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemResources;

/**
 * <p>
 * 系统菜单 服务类
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface ISystemResourcesService extends IService<SystemResources> {
	 /**
	  * 分页查询
	  * @param pageInfo
	  */
	 void selectDataGrid(PageInfo pageInfo);
	 /**
	  * 查询系统中所有的资源
	  * @author xinjun
	  * @return
	  */
	 Map<String,Object> getAllRoleResource();
	 /**
	  * 删除资源
	  * @author xinjun
	  * @return
	  */
	 Map<String,Object> deletResource(String id);
	 
	 
}
