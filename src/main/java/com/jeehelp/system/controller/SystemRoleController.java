package com.jeehelp.system.controller;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeehelp.commons.base.BaseController;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemRole;
import com.jeehelp.system.service.ISystemRoleResourcesService;
import com.jeehelp.system.service.ISystemRoleService;
/**
 * <p>
 * 系统角色 前端控制器
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
@Controller
@RequestMapping("/backgd/system/systemRole")
public class SystemRoleController extends BaseController {

    @Autowired 
    private ISystemRoleService systemRoleService;
    @Autowired 
    private ISystemRoleResourcesService roleResourcesService;
    
     /**
     * 进入分页页面
     * @return
     */
    @RequestMapping("/toList")
    public String toList() {
        return "/bg/system/systemRole/list";
    }
   
    /**
     * 查询分页数据
     * @return
     */
    @RequiresPermissions("system:systemRole:list")  
    @RequestMapping("/list")
    @ResponseBody
    public Object list(SystemRole systemRole,PageInfo pageInfo) {
       
        pageInfo.setSort("id");
        pageInfo.setOrder("desc");
        
        systemRoleService.selectDataGrid(pageInfo);
        return renderPageData(pageInfo.getRows(), pageInfo.getTotal());
    }
    
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/toAdd")
    public String toAdd() {
        return "/bg/system/systemRole/add";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @RequiresPermissions("system:systemRole:add")  
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid SystemRole systemRole) {
        systemRole.setCreateDate(new Date());
        boolean b = systemRoleService.insert(systemRole);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }
     
    /**
     * 编辑页面
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/toUpdate")
    public String toUpdate(Model model, Long id) {
        SystemRole systemRole = systemRoleService.selectById(id);
        model.addAttribute("obj", systemRole);
        return "/bg/system/systemRole/update";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @RequiresPermissions("system:systemRole:update")  
    @PostMapping("/update")
    @ResponseBody
    public Object update(@Valid SystemRole systemRole) {
        systemRole.setModifiedDate(new Date());
        boolean b = systemRoleService.updateById(systemRole);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
 
    /**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
    @RequiresPermissions("system:systemRole:delete")  
	@PostMapping("/delete")
	@ResponseBody
	public Object delete(String ids) {
		boolean b = systemRoleService.deleteBatchIds(java.util.Arrays.asList(ids.split(",")));
		if (b) {
			return renderSuccess("删除成功！");
		} else {
			return renderError("删除失败！");
		}
	}
    
    /**
	 * 跳转到修改角色权限
	 * @author xinjun
	 * @param id
	 * @return
	 */
	@RequestMapping("/toRoleResource/{id}")
	@ResponseBody
	public Object toRoleResource(@PathVariable Long id,Model model){ 
		 return systemRoleService.getRoleResource(id);
	}
	/**
	 *修改角色权限
	 * @param ids
	 * @param id
	 * @return
	 */
	@RequiresPermissions("system:systemRole:updateRoleResource")  
	@RequestMapping("/updateRoleResource")
	@ResponseBody
	public HashMap<String,Object> updateRoleResource(String ids,Long id,HttpServletRequest request){
		HashMap<String,Object> resultMap=new HashMap<String,Object>();   
		roleResourcesService.updateRoleResource(ids,id);
		resultMap.put("success", true);
		resultMap.put("msg", "保存成功！");
		return resultMap;  
	}
	
    
}
