package com.jeehelp.system.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeehelp.system.entity.SystemUser;
import com.jeehelp.system.service.ISystemUserService; 

/**
 * 后台系统登录
 * @author xinjun
 *
 * 2017年8月29日
 */
@Controller
public class LoginController {
	//private static Logger logger = Logger.getLogger(LoginController.class);
	@Autowired
	private ISystemUserService systemUserService;
	
	 /**
     * 首页
     *
     * @return
     */
    @GetMapping("/")
    public String login() {
    	Subject subject = SecurityUtils.getSubject();
		subject.logout();
        return "redirect:/login";
    }
	/**
	 * 进入登录页面
	 * 
	 * @param request
	 * @return
	 */
    @GetMapping("/login")
	public String login(Model model) {
		return "/bg/system/login";
	}

	/**
	 * 系统登录
	 * @author xinjun
	 * @param loginuser
	 * @return
	 */ 
	@PostMapping("/login")
	@ResponseBody
	public Object loginServer(SystemUser loginuser) {
		return systemUserService.loginServer(loginuser);
	} 
 

	/**
	 * 退出系统
	 * @author xinjun
	 * @param request
	 * @return
	 */
	@RequestMapping("/loginOut")
	public String loginOut() {
	 
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:/login";
	}
	
	 /**
     * 未授权
     * @return {String}
     */
    @GetMapping("/unauth")
    public String unauth() {
        if (SecurityUtils.getSubject().isAuthenticated() == false) {
            return "redirect:/login";
        } 
        return "unauth";
    } 
}
