package com.jeehelp.system.controller;

import java.util.Date;

import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeehelp.commons.base.BaseController;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemDict;
import com.jeehelp.system.service.ISystemDictService;

/**
 * <p>
 * 字典 前端控制器
 * </p>
 *
 * @author xj
 * @since 2017-06-26
 */
@Controller
@RequestMapping("/backgd/system/dict")
public class SystemDictController extends BaseController {

    @Autowired private ISystemDictService dictService;
    
     /**
     * 进入分页页面
     * @return
     */
    @RequestMapping("/toList")
    public String toList() {
    
        return "/bg/system/dict/list";
    }
     
    /**
     * 查询分页数据
     * @return
     */
    @RequiresPermissions("system:dict:list")  
    @RequestMapping("/list")
    @ResponseBody
    public Object list(SystemDict SystemDict,PageInfo pageInfo) {
     
        pageInfo.setSort("id");
        pageInfo.setOrder("desc");
        dictService.selectDataGrid(pageInfo);
        return renderPageData(pageInfo.getRows(), pageInfo.getTotal());
    }
    
    /**
     * 添加页面
     * @return
     */
    @GetMapping("/toAdd")
    public String toAdd() {
        return "/bg/system/dict/add";
    }
    
    /**
     * 添加
     * @param 
     * @return
     */
    @RequiresPermissions("system:dict:add")  
    @PostMapping("/add")
    @ResponseBody
    public Object add(@Valid SystemDict systemDict) {
    	systemDict.setCreateDate(new Date());
        boolean b = dictService.insert(systemDict);
        if (b) {
            return renderSuccess("添加成功！");
        } else {
            return renderError("添加失败！");
        }
    }
     
    /**
     * 编辑页面
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/toUpdate")
    public String toUpdate(Model model, Long id) {
        SystemDict SystemDict = dictService.selectById(id);
        model.addAttribute("obj", SystemDict);
        return "/bg/system/dict/update";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @RequiresPermissions("system:dict:update")  
    @PostMapping("/update")
    @ResponseBody
    public Object update(@Valid SystemDict systemDict) {
    	systemDict.setOperationDate(new Date());
        boolean b = dictService.update(systemDict);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
 
    /**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
    @RequiresPermissions("system:dict:delete")  
	@PostMapping("/delete")
	@ResponseBody
	public Object delete(String ids) {
		boolean b = dictService.deleteIds(ids);
		if (b) {
			return renderSuccess("删除成功！");
		} else {
			return renderError("删除失败！");
		}
	}
}
