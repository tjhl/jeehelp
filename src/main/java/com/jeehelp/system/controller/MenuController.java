package com.jeehelp.system.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeehelp.system.entity.SystemResources;
import com.jeehelp.system.service.ISystemResourcesService;

/**
 * 菜单管理
 * 
 * @author tianjy
 *
 */
@Controller
@RequestMapping("/backgd/system/menu")
public class MenuController {

	@Autowired
	private ISystemResourcesService systemResourcesService;

	/**
	 * 进入菜单管理页面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/menulist")
	public String rolelist(Model model) {
		return "/bg/system/menu/menuManage";
	}

	/**
	 * 获取所有菜单
	 * 
	 * @param request
	 * @return
	 */
	@RequiresPermissions("system:menu:getAllResource")
	@RequestMapping("/getAllResource")
	@ResponseBody
	public Object getAllRoleResource(Model model) {
		return systemResourcesService.getAllRoleResource();
	}

	/**
	 * 添加菜单
	 * 
	 * @param request
	 * @return
	 */
	@RequiresPermissions("system:menu:addResource")
	@RequestMapping("/addResource")
	@ResponseBody
	public Map<String, Object> addResource(SystemResources obj, Model model) {
		HashMap<String, Object> resultMap = new HashMap<String, Object>();

		systemResourcesService.insert(obj);
		resultMap.put("msg", "保存成功！");
		resultMap.put("success", true);
		return resultMap;
	}

	/**
	 * 修改菜单
	 * 
	 * @param request
	 * @return
	 */
	@RequiresPermissions("system:menu:updateResource")
	@RequestMapping("/updateResource")
	@ResponseBody
	public Map<String, Object> updateResource(SystemResources obj, Model model) {
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		systemResourcesService.updateById(obj);
		resultMap.put("msg", "保存成功！");
		resultMap.put("success", true);
		return resultMap;
	}

	/**
	 * 删除菜单
	 * 
	 * @param request
	 * @return
	 */
	@RequiresPermissions("system:menu:deletResource")
	@RequestMapping("/deletResource")
	@ResponseBody
	public Map<String, Object> deletResource(String id, Model model) {
		return systemResourcesService.deletResource(id);
	}

}
