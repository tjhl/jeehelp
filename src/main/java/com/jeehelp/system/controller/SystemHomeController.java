package com.jeehelp.system.controller;

 

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeehelp.system.service.ISystemUserService;
 
/**
 * 后台系统首页
 * @author xinjun
 *
 * 2017年8月29日
 */
@Controller
@RequestMapping("/backgd/system/systemHome")
public class SystemHomeController {
	@Autowired
	private ISystemUserService userService;
	 
	/**
	 *进入首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/home")
	public String home(HttpServletRequest request){ 
		return "/bg/system/index";
	}
	/**
	 * 进入修改密码页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/updatePwd") 
	public String updatePwd(){  
		return "/bg/system/pwdUpdate";
	}
	/**
	 *进入首页默认右侧展示页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/defaultPage")
	public String defaultPage(HttpServletRequest request){ 
		return "/bg/system/framework/content";
	}
	/**
	 * 编辑密码
	 * @param user
	 * @param request
	 * @return
	 */
	@RequiresPermissions("system:systemHome:updatePwdServer")  
	@RequestMapping("/updatePwdServer")
	@ResponseBody
	public Object updatePwdServer(String oldPwd,String userPass){
		return userService.updatePwdServer(oldPwd, userPass);
	}
}
