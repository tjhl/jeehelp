package com.jeehelp.system.controller;

import java.util.Date;

import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeehelp.commons.base.BaseController;
import com.jeehelp.system.entity.SystemWebsite;
import com.jeehelp.system.service.ISystemWebsiteService;

/**
 * <p>
 * 网站信息 前端控制器
 * </p>
 *
 * @author xj
 * @since 2017-07-16
 */
@Controller
@RequestMapping("/backgd/system/systemWebsite")
public class SystemWebsiteController extends BaseController {

    @Autowired private ISystemWebsiteService systemWebsiteService;
    
    /**
     * 编辑页面
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/toUpdate")
    public String toUpdate(Model model, Long id) {
        SystemWebsite systemWebsite = systemWebsiteService.selectList(null).get(0);
        model.addAttribute("obj", systemWebsite);
        return "/bg/system/systemWebsite/update";
    }
    
    /**
     * 编辑
     * @param 
     * @return
     */
    @RequiresPermissions("system:systemWebsite:update")  
    @PostMapping("/update")
    @ResponseBody
    public Object update(@Valid SystemWebsite systemWebsite) {
        systemWebsite.setModifiedDate(new Date());
        boolean b = systemWebsiteService.update(systemWebsite);
        if (b) {
            return renderSuccess("编辑成功！");
        } else {
            return renderError("编辑失败！");
        }
    }
   
}
