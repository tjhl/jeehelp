package com.jeehelp.system.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeehelp.commons.base.BaseController;
import com.jeehelp.commons.result.PageInfo;
import com.jeehelp.system.entity.SystemRole;
import com.jeehelp.system.entity.SystemUser;
import com.jeehelp.system.service.ISystemRoleService;
import com.jeehelp.system.service.ISystemUserService;

/**
 * <p>
 * 系统用户表 前端控制器
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
@Controller
@RequestMapping("/backgd/system/systemUser")
public class SystemUserController extends BaseController {

	@Autowired
	private ISystemUserService systemUserService;
	@Autowired
	private ISystemRoleService roleService;
	
	/**
	 * 进入分页页面
	 * 
	 * @return
	 */
	@RequestMapping("/toList")
	public String toList() {
		return "/bg/system/systemUser/list";
	}

	/**
	 * 查询分页数据
	 * 
	 * @return
	 */
	@RequiresPermissions("system:systemUser:list")  
	@RequestMapping("/list")
	@ResponseBody
	public Object list(SystemUser systemUser, PageInfo pageInfo) {
		systemUserService.selectDataGrid(pageInfo, systemUser);
		return renderPageData(pageInfo.getRows(), pageInfo.getTotal());
		 
	}

	/**
	 * 添加页面
	 * 
	 * @return
	 */
	@GetMapping("/toAdd")
	public String toAdd(Model model) {
		// 查询系统角色列表数据 
		List<SystemRole> roleList = roleService.selectRoleAll();
		model.addAttribute("roleList", roleList);
		return "/bg/system/systemUser/add";
	}

	/**
	 * 添加
	 * 
	 * @param
	 * @return
	 */
	@RequiresPermissions("system:systemUser:add")  
	@PostMapping("/add")
	@ResponseBody
	public Object add(@Valid SystemUser systemUser) {
		systemUser.setCreateUser(getUserId());
		boolean b = systemUserService.insertServer(systemUser);
		if (b) {
			return renderSuccess("添加成功！");
		} else {
			return renderError("添加失败！");
		}
	}

	/**
	 * 编辑页面
	 * 
	 * @param model
	 * @param id
	 * @return
	 */
	@GetMapping("/toUpdate")
	public String toUpdate(Model model, Long id) {
		// 查询系统角色列表数据 
		List<SystemRole> roleList = roleService.selectRoleAll();
		//查询用户信息
		SystemUser systemUser = systemUserService.selectById(id);
		//查询当前用户所拥有的角色列表
		List<SystemRole> userRoleList =roleService.selectRoleByUserId(id);

		model.addAttribute("obj", systemUser);
		model.addAttribute("roleList", roleList);
		model.addAttribute("userRoleList", userRoleList);
		return "/bg/system/systemUser/update";
	}

	/**
	 * 编辑
	 * 
	 * @param
	 * @return
	 */
	@RequiresPermissions("system:systemUser:update")  
	@PostMapping("/update")
	@ResponseBody
	public Object update(@Valid SystemUser systemUser) {
		systemUser.setModifiedDate(new Date());
		boolean b = systemUserService.updateServer(systemUser);
		if (b) {
			return renderSuccess("编辑成功！");
		} else {
			return renderError("编辑失败！");
		}
	}

	/**
	 * 根据id查询详情
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("/get/{id}")
	public String getUserServer(@PathVariable Long id,Model model){ 
		 
		SystemUser user=systemUserService.selectById(id);
		if(user!=null){ 
			//查询当前用户所拥有的角色列表
			List<SystemRole> userRoleList =roleService.selectRoleByUserId(id);
			model.addAttribute("userRoleList", userRoleList); 
			model.addAttribute("obj", user);
		} 
		return "/bg/system/systemUser/details";
	}
	
	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequiresPermissions("system:systemUser:delete")  
	@PostMapping("/delete")
	@ResponseBody
	public Object delete(String ids) {
		boolean b = systemUserService.deleteBatchIds(java.util.Arrays.asList(ids.split(",")));
		if (b) {
			return renderSuccess("删除成功！");
		} else {
			return renderError("删除失败！");
		}
	}
}
