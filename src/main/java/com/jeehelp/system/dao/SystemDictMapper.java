package com.jeehelp.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jeehelp.system.entity.SystemDict;

/**
 * <p>
  * 字典 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-06-25
 */
public interface SystemDictMapper extends BaseMapper<SystemDict> {

}