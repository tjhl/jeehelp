package com.jeehelp.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jeehelp.system.entity.SystemUserRole;

/**
 * <p>
  * 用户角色关系表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-07-23
 */
public interface SystemUserRoleMapper extends BaseMapper<SystemUserRole> {

}