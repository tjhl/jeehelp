package com.jeehelp.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jeehelp.system.entity.SystemResources;

/**
 * <p>
  * 系统菜单 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface SystemResourcesMapper extends BaseMapper<SystemResources> {

}