package com.jeehelp.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jeehelp.system.entity.SystemLog;

/**
 * <p>
  * 后台系统日志记录 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface SystemLogMapper extends BaseMapper<SystemLog> {

}