package com.jeehelp.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jeehelp.system.entity.SystemRole;

/**
 * <p>
  * 系统角色 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface SystemRoleMapper extends BaseMapper<SystemRole> {
	 /**
	  * 查询用户拥有的所有的角色
	  * @param userId
	  * @return
	  */
	 List<SystemRole> selectRoleByUserId(Long userId);
}