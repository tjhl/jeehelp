package com.jeehelp.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jeehelp.system.entity.SystemResources;
import com.jeehelp.system.entity.SystemRoleResources;

/**
 * <p>
  * 系统权限中间表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface SystemRoleResourcesMapper extends BaseMapper<SystemRoleResources> {
	 /**
	  * 根据角色id查询资源信息
	  * @param param
	  * @return
	  */
	 List<SystemResources> selectResourcesByT(SystemResources obj);
	 
	 /**
	  * 根据用户 id查询资源信息
	  * @param param
	  * @return
	  */
	 List<SystemResources> selectResourcesByUserId(SystemResources obj);
}