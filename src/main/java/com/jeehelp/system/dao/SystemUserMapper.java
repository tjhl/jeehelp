package com.jeehelp.system.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.jeehelp.system.entity.SystemUser;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-07-02
 */
public interface SystemUserMapper extends BaseMapper<SystemUser> {
	List<Map<String, Object>> selectUserPage(Pagination page, Map<String, Object> params);
}