package com.jeehelp.system.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.jeehelp.system.entity.SystemWebsite;

/**
 * <p>
  * 网站信息 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-07-16
 */
public interface SystemWebsiteMapper extends BaseMapper<SystemWebsite> {

}