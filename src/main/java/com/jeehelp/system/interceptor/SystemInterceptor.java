package com.jeehelp.system.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jeehelp.commons.shiro.ShiroUser;
import com.jeehelp.system.entity.SystemResources;
import com.jeehelp.system.service.ISystemRoleResourcesService;

/**
 * 后台拦截器
 * 
 * @author xj
 *
 */
@Repository
public class SystemInterceptor extends HandlerInterceptorAdapter {
	@Autowired
 	private ISystemRoleResourcesService resourcesService;
 	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String menuid = request.getParameter("menuid");
		String mpid = request.getParameter("mpid");
		if (menuid != null) {
			request.getSession().setAttribute("menuid", menuid);
			request.getSession().setAttribute("mpid", mpid);
		}  
		//防止开发阶段session 数据丢失
		if(SecurityUtils.getSubject().getPrincipal()!=null){
			ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
			if(request.getSession().getAttribute("menuOneList")==null){
				
				// 查询左侧菜单列表
				SystemResources resources=new SystemResources();
				resources.setId(shiroUser.getId());
				resources.setType(0);
		 		List<SystemResources> userResourcesList = resourcesService.selectResourcesByUserId(resources);
		 		
				List<SystemResources> menuOnelist = new ArrayList<SystemResources>();
				if (userResourcesList != null) {
					for (SystemResources resources2 : userResourcesList) {
						Long pid = resources2.getPid();
						if (pid==0) {// 如果是一级菜单
							menuOnelist.add(resources2);
						}
					}  
				}
				
				request.getSession().setAttribute("menuOneList",menuOnelist);
				request.getSession().setAttribute("menuList", userResourcesList);
			}
		} 
		return super.preHandle(request, response, handler);

	}
 
	 

}
