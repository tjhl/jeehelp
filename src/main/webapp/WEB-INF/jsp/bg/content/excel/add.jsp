<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
 
<script type="text/javascript">
    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
 
	$(document).ready(function() { 
		  $('#myform').bootstrapValidator({
			  submitHandler: function(validator, form, submitButton) {  
				  sendFileAjax("${pageContext.request.contextPath}/backgd/content/excel/upLoad","myform", function(data) {
								if (data.success) {
									layer.msg(data.msg, {icon: 1}); 
									  
								} else {
									layer.msg(data.msg, {icon: 5});
								}
						 }); 
		      }
		  })
		  
		
		
	  });
    
</script>
</head>

<body>
	<div id="contentId">
		<form class="form-horizontal" role="form" id="myform">
			 
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">导入:</label>
				<div class="col-sm-8">
					<input type="file" class="form-control"  name="file"
						placeholder="请输入字典值" data-bv-notempty data-bv-notempty-message="请上传文件">
				</div>
			</div>
			 
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">保存</button>
				</div>
			</div>
			
	
		</form>
	</div>
</body>
</html>

