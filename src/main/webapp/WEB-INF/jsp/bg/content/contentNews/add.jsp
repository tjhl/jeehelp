<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
 
<script type="text/javascript">
    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
 
    $(document).ready(function() { 
  	  $('#myform').bootstrapValidator({
  		  excluded:[":disabled"],
  		  fields: {
  			   title: {
  	                validators: {
  	                   /*  notEmpty: {
  	                        message: '姓名不能为空'
  	                    }, */
  	                    stringLength: {
  	                        min: 1,
  	                        max: 10,
  	                        message: '标题必须小于10个字'
  	                    }
  	                }
  	            },
  	            picUrl: { 
  	                validators: {
  	                    notEmpty: {
  	                        message: '请上传图片!'
  	                    }
  	                }
  	            }
  		  },
  		  submitHandler: function(validator, form, submitButton) {  
  			 
  			//手动验证
  			if($("#picUrl").val()==""){
  				validator.updateStatus("picUrl", 'VALIDATING').validateField("picUrl"); //错误提示信息变了  
  			}else{
	  			   sendAjax("${ctxPath}/backgd/content/contentNews/add", $("#myform")
						.serializeArray(), function(data) {
							if (data.success) {
								layer.msg(data.msg, {icon: 1}); 
								parent.initTableData();
							 
								setTimeout(function(){
									parent.layer.close(index);
								}, 1000);
							} else {
								layer.msg(data.msg, {icon: 5});
							}
			        });  
  			} 	
  	      }
  	  })
  	  //初始化上传图片组件
  	  webUpload('filePicker','fileList','picUrl'); 
  	  webUploadMore('filePicker1','fileList1','image1'); 
  	  initKindEditor("content");
  });
    
</script>

</head>

<body>
	<div id="contentId">
		<form class="form-horizontal" role="form" id="myform">
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">标题:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="title"  
						placeholder="请输入标题"  data-bv-notempty data-bv-notempty-message="请输入标题">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">文章类型:</label>
				<div class="col-sm-8">
				 
					<select name="type" class="selectpicker bs-select-hidden form-control"
						data-live-search="true"  data-bv-notempty data-bv-notempty-message="请选择类型">
					   <option value="">请选择</option>
					   <c:forEach var="item" items="${fns:getDictList('content_type')}">
					      <option value="${item.value}">${item.name}</option>
					   </c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
			     <label for="lastname" class="col-sm-2 control-label">主图片:</label>
			     <div class="col-sm-8">
					 <div id="fileList" class="uploader-list"></div>
	                 <div id="filePicker">选择图片</div>
	                 <input type="hidden" name="picUrl" id="picUrl">
                 </div>
			</div>
			<div class="form-group">
			     <label for="lastname" class="col-sm-2 control-label">图片:</label>
			     <div class="col-sm-8">
					 <div id="fileList1" class="uploader-list"></div>
					 <div style="clear: both;"></div>
	                 <div id="filePicker1">选择图片</div>
                 </div>
                 
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">内容:</label>
				<div class="col-sm-8">
					<textarea rows="5" cols="20" class="form-control" id="content" name="remark"
							placeholder="请输入内容">${obj.content}</textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">保存</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>

