<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include> 
<script type="text/javascript">
function operateFormatter(value, row, index) { 
	return [ 
	'<shiro:hasPermission name="system:dict:update">'+
	'<a class="btn-edit" href="javascript:openDiv(\'修改\',\'${ctxPath}/backgd/system/dict/toUpdate?id='+row.id+'\')"><span class="label label-primary">编辑</span></a>'+
	'</shiro:hasPermission>' 
    ].join('');
} 
 
function initTableData(){
	//调用函数，初始化表格 
	var tableObj = $('#cusTable'); 
	var tableConfigObj={
			id:"cusTable",
			searchFormId:"seachForm",
			url:"${ctxPath}/backgd/system/dict/list",
			singleSelect:false,
			columns: [  
		              {
		                  field: 'state',
		                  align: 'center', 
		                  checkbox: true 
		              },
		              {
		                  field: 'id', 
		                  align: 'center', 
		                  title: 'ID'
		              },
		              {
		                  field: 'name', 
		                  align: 'center', 
		                  title: '名称'
		              },
		              {
		                  field: 'value', 
		                  align: 'center', 
		                  title: '字典值'
		              },
		              {
		                  field: 'typeKey', 
		                  align: 'center', 
		                  title: '关键词'
		              },
		              {
		            	  field: 'operate',
	                      title: '操作',
	                      align: 'center', 
		                  formatter: operateFormatter
		              }
		         ]  
	};
	initTable(tableConfigObj);
}
	$(document).ready(function() {
		 
		//查询
		$("#queryBtn").click(function() {
			initTableData();
		}); 
		$("#remove").bind("click", function() { 
			delData("${ctxPath}/backgd/system/dict/delete");
		});
		initTableData();
	});

	 
</script>
</head>

<body>
	<jsp:include page="../../system/framework/top.jsp"></jsp:include>
	<jsp:include page="../../system/framework/left.jsp"></jsp:include>
	<div class="wrapper">
		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			<div class="container-fluid" style="width: 100%">
				<!-- Begin page heading -->
				<div class="line-3" style="height: 20px"></div>
				<ol class="breadcrumb default square rsaquo sm">
					<li><a data-ajax="true" data-ajax-begin="$.BodyLoadBegin"
						data-ajax-failure="$.BodyLoadFailure" data-ajax-mode="replace"
						data-ajax-success="$.BodyLoadSuccess"
						data-ajax-update="#body-content"
						href="${ctxPath}/backgd/system/systemHome/home"><i class="fa fa-home"></i></a></li>
					<li class="active">字典管理</li>

				</ol>
				<!-- Begin example member table -->
				<div class="the-box no-border">
					<nav class="navbar square navbar-primary" role="navigation">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle"
									data-toggle="collapse"
									data-target="#bs-example-navbar-collapse-7">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
								<shiro:hasPermission name="system:dict:add">
								  <a class="navbar-brand btn-add"
									href="javascript:openDiv('添加','${ctxPath}/backgd/system/dict/toAdd')"><i
									class="fa fa-plus"></i> 添加</a>
							    </shiro:hasPermission>
							    <shiro:hasPermission name="system:dict:delete">
							     <a class="navbar-brand btn-delete"
									id="remove"><i class="fa fa-trash-o"></i>
									删除</a>
                                 </shiro:hasPermission>
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse"
								id="bs-example-navbar-collapse-7">
								 
								<div class="navbar-form navbar-left">
									<form action="#"
										class="member-form"  id="seachForm"
										method="post">
										  
										<div class="form-group">
											<input class="form-control" id="key" name="key"
												placeholder="关键字" type="text" value="">
										</div>
										<button type="button" id="queryBtn" class="btn btn-default">搜索</button>
									</form>
								</div>

							</div>
							 
						</div>
						<!-- /.container-fluid -->
					</nav>


					<div class="table-responsive">
					  <table id="cusTable" data-id-field="id" class="table table-hover text-center">
	                  </table> 
					</div>
 
				</div>
				<!-- /.the-box -->
				<!-- End example member table -->


			</div>
			<!-- /.container-fluid -->
			<jsp:include page="../../system/framework/footer.jsp"></jsp:include>
		</div>
		<!-- /.page-content -->
	</div>


</body>
</html>

