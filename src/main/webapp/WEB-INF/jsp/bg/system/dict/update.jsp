<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
<script type="text/javascript">
    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
 
	$(document).ready(function() { 
		  $('#myform').bootstrapValidator({
			  submitHandler: function(validator, form, submitButton) {  
				  sendAjax("${pageContext.request.contextPath}/backgd/system/dict/update", $("#myform")
									.serializeArray(), function(data) {
								if (data.success) {
									layer.msg(data.msg, {icon: 1}); 
									parent.initTableData();
								 
									setTimeout(function(){
										parent.layer.close(index);
									}, 1000);
								} else {
									layer.msg(data.msg, {icon: 5});
								}
						 }); 
		      }
		  })
	  });
</script>
</head>

<body>
	<div id="contentId">
		<form class="form-horizontal" role="form" id="myform">
			<div class="form-group">
			    <input type="hidden" name="id" value="${obj.id}">
				<label for="firstname" class="col-sm-2 control-label">名称:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="name" name="name"
						placeholder="请输入名称" value="${obj.name}" data-bv-notempty data-bv-notempty-message="请输入名称" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">字典值:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control"  name="value"
						placeholder="请输入字典值"  value="${obj.value}" data-bv-notempty data-bv-notempty-message="请输入名称" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">字典关键词:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="typekey" name="typeKey"
						placeholder="请输入字段关键词" value="${obj.typeKey}"  data-bv-notempty data-bv-notempty-message="请输入字典关键词" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit"   class="btn btn-primary">保存</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>

