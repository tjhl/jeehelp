<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
 
<script type="text/javascript">
    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	function subFun(){
		 if($("#oldPwd").val()==""){
				layer.tips($("#oldPwd").attr("placeholder"), '#oldPwd');
		 }else if($("#userPass").val()==""){
				layer.tips($("#userPass").attr("placeholder"), '#userPass');
		 }else if($("#userPass").val()!=$("#userNewPass").val()){
				layer.tips("2次密码不一样！", '#userPass');
		 }else{
				 sendAjax("${pageContext.request.contextPath}/backgd/system/systemHome/updatePwdServer", $("#myform")
							.serializeArray(), function(data) {
						if (data.success) {
							layer.msg(data.msg, {icon: 1});  
							$("#oldPwd").val("");
							$("#userPass").val("");
							$("#userNewPass").val("");
							
						} else {
							layer.msg(data.msg, {icon: 5});
						}
				 });
			 } 
	}
	$(document).ready(function() { 
		webUpload('filePicker','fileList','filePath','${path}'); 
	})
	
</script>
 
</head>

<body>

 
			<div class="container-fluid" style="width: 100%">
				<!-- Begin page heading -->
				<div class="line-3" style="height: 20px"></div>
				<ol class="breadcrumb default square rsaquo sm">
					<li><a data-ajax="true" data-ajax-begin="$.BodyLoadBegin"
						data-ajax-failure="$.BodyLoadFailure" data-ajax-mode="replace"
						data-ajax-success="$.BodyLoadSuccess"
						data-ajax-update="#body-content"
						href="${pageContext.request.contextPath}/backgd/system/systemHome/home"><i class="fa fa-home"></i></a></li>
					<li class="active">密码修改</li>

				</ol>
				 
					 
					  <div id="contentId" class="content1">
		<form class="form-horizontal" role="form" id="myform">
			<div class="form-group">
			    <input type="hidden" name="id" value="${obj.id}">
				<label for="firstname" class="col-sm-2 control-label">原密码:</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" id="oldPwd" name="oldPwd"
						placeholder="请输入原密码" value="">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">新密码:</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" id="userPass" name="userPass"
						placeholder="请输入新密码" value="">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">确认新密码:</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" id="userNewPass" 
						placeholder="请输入新密码" value="">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">确认新密码:</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" id="userNewPass" 
						placeholder="请输入新密码" value="">
				</div>
			</div>
			
			 
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" onclick="subFun()" class="btn btn-primary">保存</button>
				</div>
			</div>
			 
		</form>
		
	 
	</div>  
			
					 

			</div>
			<!-- /.container-fluid -->
			<jsp:include page="../system/framework/footer.jsp"></jsp:include>
	 
</body>
</html>

