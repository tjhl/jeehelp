<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
<script type="text/javascript">
    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
 
	  $(document).ready(function() { 
		  $('#myform').bootstrapValidator({
			  submitHandler: function(validator, form, submitButton) {  
						  sendAjax("${ctxPath}/backgd/system/systemWebsite/update", $("#myform")
									.serializeArray(), function(data) {
								if (data.success) {
									layer.msg(data.msg, {icon: 1}); 
									 
									setTimeout(function(){
										window.location.reload();
									}, 1000);
								} else {
									layer.msg(data.msg, {icon: 5});
								}
						 }); 
		      }
		  })
	  });
    
</script>
</head>

<body>

    <jsp:include page="../framework/top.jsp"></jsp:include>
	<jsp:include page="../framework/left.jsp"></jsp:include>
	
	<div class="wrapper">
		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			<div class="container-fluid" style="width: 100%">
				<!-- Begin page heading -->
				<div class="line-3" style="height: 20px"></div>
				<ol class="breadcrumb default square rsaquo sm">
					<li><a data-ajax="true" data-ajax-begin="$.BodyLoadBegin"
						data-ajax-failure="$.BodyLoadFailure" data-ajax-mode="replace"
						data-ajax-success="$.BodyLoadSuccess"
						data-ajax-update="#body-content"
						href="${pageContext.request.contextPath}/backgd/system/systemHome/home"><i class="fa fa-home"></i></a></li>
					<li class="active">平台设置</li>

				</ol>
	
	  <div id="contentId" class="content1">
		<form class="form-horizontal" role="form" id="myform">
			<div class="form-group">
			    <input type="hidden" name="id" value="${obj.id}">
				<label for="firstname" class="col-sm-2 control-label">平台名称:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="title"
				    value="${obj.title}" placeholder="请输入平台名称" data-bv-notempty data-bv-notempty-message="请输入平台名称" data-bv-stringlength="true" 
						 data-bv-stringlength-max="10" data-bv-stringlength-message="长度不得超过10">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">公司名称:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="company"
					  value="${obj.company}" placeholder="请输入公司名称" data-bv-notempty data-bv-notempty-message="请输入公司名称" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">备案号:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="beian"
					  value="${obj.beian}" placeholder="请输入备案号" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">版权:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="copyright"
					  value="${obj.copyright}" placeholder="请输入版权" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">保存</button>
				</div>
			</div>
		</form>
	</div>
	
		</div>
			<!-- /.container-fluid -->
			<jsp:include page="../../system/framework/footer.jsp"></jsp:include>
		</div>
		<!-- /.page-content -->
	</div>
	
</body>
</html>

