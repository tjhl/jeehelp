﻿<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="Sentir, Responsive admin and dashboard UI kits template">
<meta name="keywords"
	content="admin,bootstrap,template,responsive admin,dashboard template,web apps template">
<meta name="author"
	content="Ari Rusmanto, Isoh Design Studio, Warung Themes">
<jsp:include page="/commons/style.jsp"></jsp:include>
 
<style type="text/css">
 .xline {
    border-bottom: solid 1px #dfe9ee;
    height: 5px;
}
.welinfo i {
    font-style: normal;
    padding-left: 8px;
}
.mainindex {
    padding: 20px;
    overflow: hidden;
    background-color: #FFFFFF;font-family: '微软雅黑';
}
.welinfo b {
    padding-left: 8px;
}

 html, body

        {

            margin: 0px 0px;

            width: 100%;

            height: 100%;

        }

        iframe

        {

            margin: 0px 0px;

            width: 100%;

            height: 100%;

        }
</style> 
</head>

<body>
	<c:set var="menuCheck" scope="session" value="1"></c:set>
	<jsp:include page="framework/top.jsp"></jsp:include>
	<div>
	     <!-- BEGIN SIDEBAR LEFT -->
			<div class="sidebar-left sidebar-nicescroller">
				<ul class="sidebar-menu">
					  
					<c:forEach var="item" items="${menuOneList}">
					   <li>
						<c:if test="${empty item.url}">
						<a href="#fakelink">
						<i class="fa fa-table icon-sidebar"></i>
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							${item.name}
						</a> 
						</c:if>
						<c:if test="${!empty item.url}">
						<a href="${ctxPath}/${item.url}?menuid=${item.id}&mpid=${item.pid}">
						<i class="fa fa-table icon-sidebar"></i>
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							${item.name}
						</a> 
						</c:if>
						
							
						<ul class="submenu">
							<c:forEach var="sitem" items="${menuList}">
							   <c:if test="${item.id==sitem.pid}">
							     <li><a href="${ctxPath}/${sitem.url}" target="rightFrame">${sitem.name}</a></li> 
						       </c:if>
						    </c:forEach>
						</ul>
					  </li> 
					</c:forEach> 
				</ul>
			</div><!-- /.sidebar-left -->
			<!-- END SIDEBAR LEFT -->
			
			<div class="wrapper" style="height: 100%">
		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content" style="height: 100%" id="page-content">
           <iframe  id="rightFrame" name="rightFrame" style="width:100%; min-height:400px;overflow:visible;background:#fff;" width="100%" height="100%" src="${pageContext.request.contextPath}/backgd/system/systemHome/defaultPage" frameborder="0"></iframe>
        </div>
          </div>
	</div>
	 
	<script>
 
	window.onload = function () {
		 $("#rightFrame").load(function () {
			var mainheight = $(this).contents().find("body").height() +50;
			 //alert(window.screen.height );
			 $(this).height(window.screen.height-160);
			});  
		  
	};
 </script>


</body>
</html>