<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
<title>后台管理系统</title>
<script type="text/javascript">
    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
 
	$(document).ready(function() { 
		  $('#myform').bootstrapValidator({
			  submitHandler: function(validator, form, submitButton) {  
					 sendAjax("${pageContext.request.contextPath}/backgd/system/systemUser/update", $("#myform")
									.serializeArray(), function(data) {
								if (data.success) {
									layer.msg(data.msg, {icon: 1}); 
									parent.initTableData();
								 
									setTimeout(function(){
										parent.layer.close(index);
									}, 1000);
								} else {
									layer.msg(data.msg, {icon: 5});
								}
						 }); 
		      }
		  })
	  });
</script>
</head>

<body>
	<div id="contentId">
		<form class="form-horizontal" role="form" id="myform">
			<div class="form-group">
			    <input type="hidden" name="id" value="${obj.id}">
				<label for="firstname" class="col-sm-2 control-label">账号:</label>
				<div class="col-sm-8"> 
					<input type="text" class="form-control" id="userpass" name="userpass"
						  value="${obj.userName}" disabled="disabled">
				</div>
				 
			</div>
			 
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">姓名:</label>
				<div class="col-sm-8"> 
					<input type="text" class="form-control" id="realName" name="realName"
						placeholder="请输入姓名" data-bv-notempty data-bv-notempty-message="请输入姓名" value="${obj.realName}" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">角色:</label> 
				<div class="col-sm-8">
					 <c:forEach var="objItem" items="${roleList}">
					     <c:set var="ischeck" value="false"></c:set>
					     <c:forEach var="userObjItem" items="${userRoleList}">
					         <c:if test="${objItem.id==userObjItem.id}">
					             <c:set var="ischeck" value="true"></c:set>
					         </c:if>
					     </c:forEach>
					     
					     <input type="checkbox" name="roleIds" value="${objItem.id}" <c:if test="${ischeck}">checked="checked"</c:if>>${objItem.name}
					 </c:forEach>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">保存</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>

