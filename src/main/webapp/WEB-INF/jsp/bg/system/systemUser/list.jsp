<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>e>
<script type="text/javascript">
function operateFormatter(value, row, index) { 
	return [ 
         '<a class="btn-edit"'+
		'href="javascript:openDiv(\'详情\',\'${ctxPath}/backgd/system/systemUser/get/'+row.id+'\')">详情</a>&nbsp;'+
	'<shiro:hasPermission name="system:systemUser:update">'+
	'<a class="btn-edit" href="javascript:openDiv(\'编辑\',\'${ctxPath}/backgd/system/systemUser/toUpdate?id='+row.id+'\')">编辑</a>'+
	'</shiro:hasPermission>' 
    ].join('');
} 
 
function initTableData(){
	//调用函数，初始化表格 
	var tableObj = $('#cusTable'); 
	var tableConfigObj={
			id:"cusTable",
			searchFormId:"seachForm",
			url:"${ctxPath}/backgd/system/systemUser/list",
			singleSelect:false,
			columns: [  
		              {
		                  field: 'state',
		                  align: 'center', 
		                  checkbox: true 
		              },
		              {
		                  field: 'id', 
		                  align: 'center', 
		                  title: 'ID'
		              },
		              {
		                  field: 'userName', 
		                  align: 'center', 
		                  title: '账号'
		              },
		              {
		                  field: 'realName', 
		                  align: 'center', 
		                  title: '姓名'
		              }, 
		              
		              {
		            	  field: 'operate',
	                      title: '操作',
	                      align: 'center', 
		                  formatter: operateFormatter
		              }
		         ]  
	};
	initTable(tableConfigObj);
}
	$(document).ready(function() {
		 
		//查询
		$("#queryBtn").click(function() {
			initTableData();
		}); 
		$("#remove").bind("click", function() { 
			delData("${ctxPath}/backgd/system/systemUser/delete");
		});
		initTableData();
	});

	 
</script>
</head>

<body>

	<jsp:include page="../framework/top.jsp"></jsp:include>
	<jsp:include page="../framework/left.jsp"></jsp:include>
	<div class="wrapper">
		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			<div class="container-fluid" style="width: 100%">
				<!-- Begin page heading -->
				<div class="line-3" style="height: 20px"></div>
				<ol class="breadcrumb default square rsaquo sm">
					<li><a data-ajax="true" data-ajax-begin="$.BodyLoadBegin"
						data-ajax-failure="$.BodyLoadFailure" data-ajax-mode="replace"
						data-ajax-success="$.BodyLoadSuccess"
						data-ajax-update="#body-content"
						href="${ctxPath}/backgd/system/systemHome/home"><i class="fa fa-home"></i></a></li>
					<li class="active">用户管理</li>

				</ol>
				<!-- Begin example member table -->
				<div class="the-box no-border">
					<nav class="navbar square navbar-primary" role="navigation">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle"
									data-toggle="collapse"
									data-target="#bs-example-navbar-collapse-7">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
								 <shiro:hasPermission name="system:systemUser:add">
								  <a class="navbar-brand btn-add"
									href="javascript:openDiv('添加用户','${ctxPath}/backgd/system/systemUser/toAdd')"><i
									class="fa fa-plus"></i> 添加</a>
							     </shiro:hasPermission>
							     <shiro:hasPermission name="system:systemUser:delete">
							     <a class="navbar-brand btn-delete"
									id="remove"><i class="fa fa-trash-o"></i>
									删除</a>
                                 </shiro:hasPermission>
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse"
								id="bs-example-navbar-collapse-7">
								 
								<div class="navbar-form navbar-left">
									<form action="#"
										class="member-form"  id="seachForm"
										method="post"> 
										<div class="form-group">
											<input class="form-control" id="key" name="key"
												placeholder="关键字" type="text" value="">
										</div>
										<button type="button" id="queryBtn" class="btn btn-default">搜索</button>
									</form>
								</div>

							</div>
							 
						</div>
						<!-- /.container-fluid -->
					</nav>


					<div class="table-responsive">
					  <table id="cusTable" data-id-field="id" class="table table-hover text-center">
	                  </table> 
					</div>
 
				</div>
				<!-- /.the-box -->
				<!-- End example member table -->


			</div>
			<!-- /.container-fluid -->
			<jsp:include page="../../system/framework/footer.jsp"></jsp:include>
		</div>
		<!-- /.page-content -->
	</div>


</body>
</html>

