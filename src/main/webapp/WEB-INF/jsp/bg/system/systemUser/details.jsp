<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
 <style type="text/css">
   .form-group{
      border: 1px solid #eeeeee;
   }
 </style>
</head>

<body>
	<div id="contentId">
		<form class="form-horizontal" role="form" id="myform">
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">账号:</label>
				<div class="col-sm-8  text-control">
					 ${obj.userName}
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">姓名:</label>
				<div class="col-sm-8  text-control">
					 ${obj.realName}
				</div>
			</div>
			 <div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">角色:</label>
				<div class="col-sm-8  text-control">
					 <c:forEach var="objItem" items="${userRoleList}">
					     ${objItem.name}
					 </c:forEach>
				</div>
			</div>
		</form>
	</div>
</body>
</html>

