<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
<script type="text/javascript">
    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
 
	 $(document).ready(function() { 
		  $('#myform').bootstrapValidator({
			  submitHandler: function(validator, form, submitButton) {  
					 sendAjax("${pageContext.request.contextPath}/backgd/system/systemUser/add", $("#myform")
									.serializeArray(), function(data) {
								if (data.success) {
									layer.msg(data.msg, {icon: 1}); 
									parent.initTableData();
								 
									setTimeout(function(){
										parent.layer.close(index);
									}, 1000);
								} else {
									layer.msg(data.msg, {icon: 5});
								}
						 }); 
		      }
		  })
	  });
</script>
</head>

<body>
	<div id="contentId">
		<form class="form-horizontal" role="form" id="myform">
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">账号:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="userName" name="userName"
						placeholder="请输入账号" data-bv-notempty data-bv-notempty-message="请输入账号" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">密码:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="userPass" name="userPass"
						placeholder="请输入密码" data-bv-notempty data-bv-notempty-message="请输入密码" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">姓名:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="realName" name="realName"
						placeholder="请输入姓名" data-bv-notempty data-bv-notempty-message="请输入姓名" data-bv-stringlength="true" 
						 data-bv-stringlength-max="30" data-bv-stringlength-message="长度不得超过30">
				</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">角色:</label>
				<div class="col-sm-8">
					 <c:forEach var="objItem" items="${roleList}">
					     <input type="checkbox" name="roleIds" value="${objItem.id}">${objItem.name}
					 </c:forEach>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit"   class="btn btn-primary">保存</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>

