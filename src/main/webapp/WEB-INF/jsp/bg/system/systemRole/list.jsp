<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
     <link rel="stylesheet" href="${ctxPath}/style/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css"/>	 
    <script type="text/javascript" src="${ctxPath}/style/admin/assets/js/ztree/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${ctxPath}/style/admin/assets/js/ztree/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript">
function operateFormatter(value, row, index) { 
	return [ 
         '<shiro:hasPermission name="system:systemRole:updateRoleResource"><a class="btn-edit"'+
		'href="javascript:getRoleRes('+row.id+')">权限</a></shiro:hasPermission>&nbsp;'+
	'<shiro:hasPermission name="system:systemRole:update">'+
	'<a class="btn-edit" href="javascript:openDiv(\'编辑\',\'${ctxPath}/backgd/system/systemRole/toUpdate?id='+row.id+'\')">编辑</a>'+
	'</shiro:hasPermission>' 
    ].join('');
} 
 
function initTableData(){
	//调用函数，初始化表格 
	var tableObj = $('#cusTable'); 
	var tableConfigObj={
			id:"cusTable",
			searchFormId:"seachForm",
			url:"${ctxPath}/backgd/system/systemRole/list",
			singleSelect:false,
			columns: [  
		              {
		                  field: 'state',
		                  checkbox: true,
		                  align: 'center'
		              },
		              {
		                  field: 'id', 
		                  title: 'ID',
		                  align: 'center'
		              },
		              {
		                  field: 'name', 
		                  title: '名称',
		                  align: 'center'
		              }, 
		              {
		                  field: 'modifiedDate', 
		                  title: '操作日期',
		                  align: 'center' 
		              },
		              {
		                  field: 'remark', 
		                  title: '备注',
		                  align: 'center'
		              }, 
		              {
		            	  field: 'operate',
	                      title: '操作',
	                      align: 'center', 
		                  formatter: operateFormatter
		              }
		         ]  
	};
	initTable(tableConfigObj);
}
	$(document).ready(function() {
		 
		//查询
		$("#queryBtn").click(function() {
			initTableData();
		}); 
		$("#remove").bind("click", function() { 
			delData("${ctxPath}/backgd/system/systemRole/delete");
		});
		initTableData();
	});

	var setting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};  
   var mid="";
   function getRoleRes(id){
    	mid=id;
    	$("#openId").click();
    	sendAjax(
				"${ctxPath}/backgd/system/systemRole/toRoleResource/"+id,
				 null, function(data) {
					if (data.success) {
						$.fn.zTree.init($("#treeDemo"), setting, data.data);
					}else{
						layer.msg("查询失败！", {icon: 5}); 
	                }
				}
		); 
    }
   function updateFun(){
	   var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
       var nodes = treeObj.getCheckedNodes(true);
       var ids = "";
       for (var i = 0; i < nodes.length; i++) {
           if(i==0){
        	   ids=nodes[i].id;
           }else{
        	   ids=ids+","+nodes[i].id;
           }
       }
   	sendAjax(
   			"${ctxPath}/backgd/system/systemRole/updateRoleResource?ids="+ids+"&id="+mid, 
			 null, function(data) {
				if (data.success) {
					layer.msg("保存成功！", {icon:1});  
	   				$(".mfp-close").click();
				}else{
					layer.msg("服务器失败！", {icon: 5}); 
                }
			}
	); 
   	
    
   }
</script>
<style>
  ul.ztree {margin-top: 10px;border: 1px solid #617775;background: #f0f6e4;width:320px;height:360px;overflow-y:scroll;overflow-x:auto;}
  .ztree span{
    display:inline-block;
  }
</style>
</head>

<body>
   
	<jsp:include page="../framework/top.jsp"></jsp:include>
	<jsp:include page="../framework/left.jsp"></jsp:include>
	 	
      
	<div class="wrapper">
		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			<div class="container-fluid" style="width: 100%">
				<!-- Begin page heading -->
				<div class="line-3" style="height: 20px"></div>
				<ol class="breadcrumb default square rsaquo sm">
					<li><a data-ajax="true" data-ajax-begin="$.BodyLoadBegin"
						data-ajax-failure="$.BodyLoadFailure" data-ajax-mode="replace"
						data-ajax-success="$.BodyLoadSuccess"
						data-ajax-update="#body-content"
						href="${ctxPath}/backgd/system/systemHome/home"><i class="fa fa-home"></i></a></li>
					<li class="active">角色管理</li>

				</ol>
				 
							<div class="the-box">
								 
								<ul class="inline-popups" style="display: none;"> 
									<li><a id="openId" href="#text-popup-html" data-effect="mfp-zoom-in">Form popup</a></li> 
								</ul> 
								<!-- Form popup -->
								<div id="text-popup-html" class="white-popup mfp-with-anim mfp-hide">
									   <ul id="treeDemo" class="ztree"></ul>  
									  <button type="button" class="btn btn-primary" onclick="updateFun()">修改权限</button>
								</div> 
							</div>
				 
				<!-- Begin example member table -->
				<div class="the-box no-border">
					<nav class="navbar square navbar-primary" role="navigation">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle"
									data-toggle="collapse"
									data-target="#bs-example-navbar-collapse-7">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
							   <shiro:hasPermission name="system:systemRole:add">
								  <a class="navbar-brand btn-add"
									href="javascript:openDiv('添加角色','${ctxPath}/backgd/system/systemRole/toAdd')"><i
									class="fa fa-plus"></i> 新增</a>
							   </shiro:hasPermission>
							   <shiro:hasPermission name="system:systemRole:delete">
							        <a class="navbar-brand btn-delete"
									id="remove"><i class="fa fa-trash-o"></i>
									删除</a>
                               </shiro:hasPermission> 
							</div>
							
							<div class="collapse navbar-collapse"
								id="bs-example-navbar-collapse-7">
								 
								<div class="navbar-form navbar-left">
									<form action="#"
										class="member-form"  id="seachForm"
										method="post">
										   
									</form>
								</div>

							</div>
							 
						</div>
						<!-- /.container-fluid -->
					</nav>

                     <div class="table-responsive">
					  <table id="cusTable" data-id-field="id" class="table table-hover text-center">
	                  </table>
	                  </div>

				</div>
				<!-- /.the-box -->
				<!-- End example member table -->


			</div>
			<!-- /.container-fluid -->
			<jsp:include page="../../system/framework/footer.jsp"></jsp:include>
		</div>
		<!-- /.page-content -->
	</div>


</body>
</html>

