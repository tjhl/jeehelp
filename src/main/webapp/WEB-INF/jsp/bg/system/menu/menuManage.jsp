<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="/commons/style.jsp"></jsp:include>
<title>后台管理系统</title>
     <link rel="stylesheet" href="${pageContext.request.contextPath}/style/admin/assets/css/zTreeStyle/zTreeStyle.css" type="text/css"/>	 
    <script type="text/javascript" src="${pageContext.request.contextPath}/style/admin/assets/js/ztree/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/style/admin/assets/js/ztree/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript">

var menuName="";var menuId = "";var menuPid="";var menuType=""
 var menuUrl="";var menuSort="",menuPermission="";

	$(document).ready(function() {
		getAllResource();
		
		$("#menuadd").hide();
	    $("#menuupdate").hide();
	    
		$("#menuAddDate").click(function(){
			
			$("#cradio").removeAttr("checked");
			$("#aradio").removeAttr("checked");
			
		    $("#aname").val("");
		    $("#apaixu").val("");
		    $("#aaddress").val("");
			
			$("#menuadd").show();
		    $("#menuupdate").hide();
		    
		    var  $androidDialog2 = $('#androidDialog2');
		    $androidDialog2.hide();
		    
		    $("#asj").html(menuName);
		    
		});
		
		$("#menuUpdates").click(function(){
			
			$("#menuadd").hide();
		    $("#menuupdate").show();
		    
		    var  $androidDialog2 = $('#androidDialog2');
		    $androidDialog2.hide();
		    
		    if(menuType==0){
		    	$("#radioc").attr("checked", "checked"); 
		    }else if(menuType==1){
		    	$("#radion").attr("checked", "checked"); 
		    }
		    $("#uname").val(menuName);
		    $("#upaixu").val(menuSort);
		    $("#uaddress").val(menuUrl);
		    $("#upermission").val(menuPermission);
		});
		
		$("#asave").click(function(){
			addMenu();
		});
		
		$("#usave").click(function(){
			updateMenu();
		});
		
		$("#menuDelet").click(function(){
			var  $androidDialog2 = $('#androidDialog2');
		    $androidDialog2.hide();
			deletMenu();
		});
		
		
	});

	var setting = {
			data: {
				simpleData: {
					enable: true
				},
				key: {
					url: false
				}
			},
			callback:{
				onClick: onAreaTree
			}
		};  
	
   function getAllResource(){
    	sendAjax(
				"${pageContext.request.contextPath}/backgd/system/menu/getAllResource",
				 null, function(data) {
					if (data.success) {
						$.fn.zTree.init($("#treeDemo"), setting, data.data);
					}else{
						layer.msg("查询失败！", {icon: 5}); 
	                }
				}
		); 
    }
   
   
   function addMenu(){
	   
	   var group = $("[name='group1']").filter(":checked"); 
	   var caidan = group.val();
	   var aname  = $("#aname").val();
	   var apaixu = $("#apaixu").val();
	     
	   $("#apid").val(menuId);
	   if(aname==""){
			layer.tips('请输入名称!', '#aname');
		}else if(apaixu==""){
			layer.tips('请输入排序!', '#apaixu');
		}else if(isNaN(apaixu)){
			layer.tips('请输入排序（数字）!', '#apaixu');
		}else{
			sendAjax(
		   			"${pageContext.request.contextPath}/backgd/system/menu/addResource", 
		   		 $("#addform").serializeArray(), function(data) {
						if (data.success) {
							layer.msg("保存成功！", {icon:1});  
							location.reload();
						}else{
							layer.msg("服务器失败！", {icon: 5}); 
		                }
					}
			); 
		}
	   
	   
	   
   }
   
	function updateMenu(){
	        
		   var uname  = $("#uname").val();
		   var upaixu = $("#upaixu").val();
		   $("#uid").val(menuId);
		   
		    if(uname==""){
				layer.tips('请输入名称!', '#uname');
			}else if(upaixu==""){
				layer.tips('请输入排序!', '#upaixu');
			}else if(isNaN(upaixu)){
				layer.tips('请输入排序（数字）!', '#upaixu');
			}else{
				sendAjax(
			   			"${pageContext.request.contextPath}/backgd/system/menu/updateResource", 
						 $("#updateform").serializeArray(), function(data) {
							if (data.success) {
								layer.msg("保存成功！", {icon:1});  
								location.reload();
							}else{
								layer.msg("服务器失败！", {icon: 5}); 
			                }
						}
				); 
			}
		   
	   }
	
	
	function deletMenu(){
		sendAjax(
	   			"${pageContext.request.contextPath}/backgd/system/menu/deletResource?id="+menuId, 
				 null, function(data) {
					if (data.success) {
						layer.msg("删除成功！", {icon:1});  
						location.reload();
					}else{
						var  $iosDialog2 = $('#iosDialog2');
					    $iosDialog2.fadeIn(200);
	                }
				}
		); 
	 }
	
	function canlAlt(){
		var  $iosDialog2 = $('#iosDialog2');
	    $iosDialog2.hide();
	}
	
	function onAreaTree(e, treeId, treeNode) {
        
		var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
        nodes = zTree.getSelectedNodes(),
        v = "";
		
        nodes.sort(function compare(a, b) { return a.id - b.id; });
        for (var i = 0, l = nodes.length; i < l; i++) {
            v += nodes[i].name + ",";
            
            menuId = nodes[i].id;
            menuPid = nodes[i].pid;
            menuType = nodes[i].type;
            menuUrl = nodes[i].url;
            menuSort = nodes[i].sort;
            menuPermission=nodes[i].permission;
            
        }
        
        menuName=treeNode.name; 

        var  $androidDialog2 = $('#androidDialog2');
        $androidDialog2.fadeIn(200);
        
        return false;
		
    }
   
   
   
</script>
<style>
  ul.ztree {margin-top: 10px;border: 1px solid #617775;background: #f0f6e4;width:320px;overflow-y:scroll;overflow-x:auto;}
  .ztree span{
    display:inline-block;
  }
</style>
</head>

<body>
   
	 
			<div class="container-fluid" style="width: 100%">
				<!-- Begin page heading -->
				<div class="line-3" style="height: 20px"></div>
				<ol class="breadcrumb default square rsaquo sm">
					<li><a data-ajax="true" data-ajax-begin="$.BodyLoadBegin"
						data-ajax-failure="$.BodyLoadFailure" data-ajax-mode="replace"
						data-ajax-success="$.BodyLoadSuccess"
						data-ajax-update="#body-content"
						href="${pageContext.request.contextPath}/backgd/system/systemHome/home"><i class="fa fa-home"></i></a></li>
					<li class="active">菜单管理</li>

				</ol>
				 
				 
				<!-- Begin example member table -->
				<div class="the-box no-border">
					<nav class="navbar square navbar-primary" role="navigation">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle"
									data-toggle="collapse"
									data-target="#bs-example-navbar-collapse-7">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
								
								<a class="navbar-brand"
									id="remove"><i class="fa"></i>
									菜单管理</a>
								
							</div>
							
						</div>
						<!-- /.container-fluid -->
					</nav>

                    <div style="width: 100%;">
                    
                    	<div style="float: left;width: 40%" class="the-box">
							<!-- Form popup -->
							<div id="text-popup-html" class="white-popup">
								<ul id="treeDemo" class="ztree"></ul>  
							</div> 
						 </div>
						 
						 <div style="float: left;width: 60%;margin-top: 30px">
						 
						   <div style="width: 100%;text-align: center;">
						        <form class="form-horizontal" role="form" id="addform">
							   	<div id="menuadd" style="width: 100%;text-align: center;margin-top: 20px;">
							   	
								   		<div style="width: 100%;height: 40px;text-align: center;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">上级：</div>
									   		<div id="asj" style="width: 90%;height: 40px;text-align: center;line-height: 40px;float: left;color: #000;font-style: normal;"></div>
									   		<div style="clear: both;" ></div>
									   	     <input type="hidden" id="apid" name="pid">
									   	</div>
									   	
									   	<div style="width: 100%;height: 40px;text-align: center;margin-top: 15px;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">名称：</div>
									   		<div style="width:90%;height: 40px;text-align: center;line-height: 40px;float: left;">
									   			<label style="height: 38px;width:90%;"><input style="height: 38px;width:100%;"  class="form-control" id="aname" name="name" type="text" value="" placeholder="请输入名称"/> </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
								   	
									   	<div style="width: 100%;height: 40px;text-align: center;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">类型：</div>
									   		<div style="width:45%;height: 40px;text-align: center;line-height: 40px;float: left;">
									   			<label style="width: 100%"><input id="cradio" name="type" type="radio" checked="checked" value="0" />菜单 </label> 
									   		</div>
									   		<div style="width:45%;;height: 40px;text-align: center;line-height: 40px;float: left;">
									   			<label style="width: 100%"><input id="aradio" name="type" type="radio" value="1" />按钮 </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
									   	
									   	<div style="width: 100%;height: 40px;text-align: center;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">地址：</div>
									   		<div style="width:90%;height: 40px;line-height: 40px;float: left;">
									   			<label style="height: 38px;width:90%;"><input style="height: 38px;width:100%;" class="form-control" id="aaddress"  name="url" type="text" value="" placeholder="请输入地址"/> </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
									   	
									   	<div style="width: 100%;height: 40px;text-align: center;margin-top: 15px;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">权限：</div>
									   		<div style="width:90%;height: 40px;line-height: 40px;float: left;">
									   			<label style="height: 38px;width:90%;"><input class="form-control"  style="height: 38px;width:100%;" name="permission" type="text" value="" placeholder="请输入排序"/> </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
									   	<div style="width: 100%;height: 40px;text-align: center;margin-top: 15px;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">排序：</div>
									   		<div style="width:90%;height: 40px;line-height: 40px;float: left;">
									   			<label style="height: 38px;width:90%;"><input class="form-control" id="apaixu" style="height: 38px;width:100%;" name="sort" type="text" value="" placeholder="请输入排序"/> </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
									   	<div style="width: 100%;height: 30px;text-align: center;height: 30px;margin-top: 15px;text-align: center;line-height: 30px;">
									   		<label id="asave" style="width: 60px;height: 30px;background: #37BC9B;text-align: center;line-height: 30px;color: #fff;font-size: 14px;">保存</label>
									   		<div style="clear: both;" ></div>
									   </div>
							   	
							   	</div>
						   		</form>
						   		<form class="form-horizontal" role="form" id="updateform">
						   		<div id="menuupdate" style="width: 100%;text-align: center;margin-top: 20px;">
						   		
						   			<div style="width: 100%;height: 40px;text-align: center;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">类型：</div>
									   		<div style="width: 45%;height: 40px;text-align: center;line-height: 40px;float: left;">
									   			<label style="width: 100%"><input id="radioc" name="type" type="radio" value="0" />菜单 </label> 
									   		</div>
									   		<div style="width: 45%;height: 40px;text-align: center;line-height: 40px;float: left;">
									   			<label style="width: 100%"><input  id="radion" name="type" type="radio" value="1" />按钮 </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
									   	<input type="hidden" id="uid" name="id"/>
									   	<div style="width: 100%;height: 40px;text-align: center;margin-top: 15px;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">名称：</div>
									   		<div style="width:90%;height: 40px;text-align: center;line-height: 40px;float: left;">
									   			<label style="width:90%;"><input class="form-control" id="uname" style="height: 40px;width:90%;" name="name" type="text" value="" placeholder="请输入名称"/> </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
									   	
									   	<div style="width: 100%;height: 40px;text-align: center;margin-top: 15px;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">地址：</div>
									   		<div style="width:90%;height: 40px;text-align: center;line-height: 40px;float: left;">
									   			<label style="width:90%;"><input class="form-control" id="uaddress" name="url" style="height: 40px;width:90%;"  type="text" value="" placeholder="请输入地址"/> </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
									   	<div style="width: 100%;height: 40px;text-align: center;margin-top: 15px;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">权限：</div>
									   		<div style="width:90%;height: 40px;line-height: 40px;float: left;">
									   			<label style="height: 38px;width:90%;"><input class="form-control" id="upermission" style="height: 38px;width:100%;" name="permission" type="text" value="" placeholder="请输入权限地址"/> </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
									   	<div style="width: 100%;height: 40px;text-align: center;margin-top: 15px;">
									   		<div style="width: 10%;height: 40px;text-align: center;line-height: 40px;float: left;">排序：</div>
									   		<div style="width:90%;height: 40px;text-align: center;line-height: 40px;float: left;">
									   			<label style="width:90%;"><input class="form-control" id="upaixu" style="height: 40px;width:90%;" name="sort" type="text" value="" placeholder="请输入排序"/> </label> 
									   		</div>
									   		<div style="clear: both;" ></div>
									   	</div>
									   	
									   	<div style="width: 100%;height: 30px;text-align: center;height: 30px;margin-top: 15px;">
									   		<label id="usave" style="width: 60px;height: 30px;background: #37BC9B;text-align: center;line-height: 30px;color: #fff;font-size: 14px;float: left;margin-left: 90px">保存</label>
									   		<div style="clear: both;" ></div>
									   </div>
						   		
						   		</div>
						        </form>.
						   </div>
						 
						 
						 </div>
                    
                    	<div style="clear: both;" ></div>
                    	
                    </div>
	                  
	                  
	                  

				</div>
				<!-- /.the-box -->
				<!-- End example member table -->


			</div>
			<!-- /.container-fluid -->
		 

		<!--BEGIN dialog4-->
		        <div class="js_dialog" id="androidDialog2" style="display: none;">
		            <div class="weui-mask"></div>
		            <div class="weui-dialog weui-skin_android">
		                <div class="weui-dialog__bd">
		                    请选择操作
		                </div>
		                <div class="weui-dialog__ft">
		                    <a id="menuAddDate"  class="weui-dialog__btn weui-dialog__btn_default">添加</a>
		                    <a id="menuUpdates"  class="weui-dialog__btn weui-dialog__btn_primary">修改</a>
		                    <a id="menuDelet" style="color: #ff0000"  class="weui-dialog__btn weui-dialog__btn_primary">删除</a>
		                </div>
		            </div>
		        </div>
        <!--END dialog4-->
        
        
        <!--BEGIN dialog2-->
        <div class="js_dialog" id="iosDialog2" style="display: none;">
            <div class="weui-mask"></div>
            <div class="weui-dialog">
                <div class="weui-dialog__bd">对不起因为此菜单有下级菜单，不能删除</div>
                <div class="weui-dialog__ft">
                    <a onclick="canlAlt()" href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary">知道了</a>
                </div>
            </div>
        </div>
        <!--END dialog2-->
        



</body>
</html>

