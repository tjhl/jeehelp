/* show message */
function showMessage(msg, bl) {
	$("#msgBoxDIV").fadeIn(100);
	setTimeout(function() {
		$("#msgBoxDIV").fadeOut(1000)
	}, 2000);
	if (bl) {
		$(".msg").css({
			background : "#68af02"
		});
	} else {
		$(".msg").css({
			background : "#ef8f00"
		});
	}
	$(".msg").text((msg + "").replace("Error:", ""));
}
/**
 * 公用ajax form表单提交
 * 
 * @param url
 * @param data
 * @param callback
 */
function sendAjax(url, data, callback) {
	layer.load();
	$.ajax({
		type : 'POST',
		dataType : "json",
		url : url,
		data : data,
		success : function(data, status, xhr) {
			layer.closeAll('loading');
			if (callback) {
				callback(data);
			}
		},
		error : function(xhq, textStatus, erroThrown) {
			layer.closeAll('loading');
			if (xhq.status == 0) {
				alert("请求失败！");
			} else if (xhq.status == 404) {
				alert("服务器异常！");
			} else if (xhq.status == 500) {
				alert("服务器内部错误！");
			} else if (xhq.status == 200) {
				alert("会话失效，请重新登入");
				window.location.reload(false);
			} else {
				alert("错误：" + xhq.status);
			}
		}
	});

}
/**
 * 公用ajax form表单提交支持包括文件
 * 
 * @param formId
 * @param url
 * @param data
 * @param callback
 */
function sendFileAjax(url,formId, callback) {
	layer.load();
	$("#" + formId).ajaxSubmit({
		type : 'post',
		url : url,
		dataType : "json",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(data) {
			layer.closeAll('loading');
			if (callback) {
				callback(data);
			}
		}
	});
}
 

function openDiv(title, url) {
	layer.open({
		type : 2,
		move : false, // 关闭拖拽
		skin : 'layui-layer-rim', // 加上边框
		area : [ '65%', '80%' ], // 宽高
		title : title,
		content : url
	});
}
function trStyleOver(obj) {
	$(obj).css("background-color", "#96FED1");
}
function trStyleOut(obj) {
	$(obj).css("background-color", "transparent");
}
function timeStamp2String(time) {
	if (time == null || time == "") {
		return "";
	}
	var datetime = new Date();
	datetime.setTime(time);
	var year = datetime.getFullYear();
	var month = datetime.getMonth() + 1;
	var date = datetime.getDate();
	var hour = datetime.getHours();
	var minute = datetime.getMinutes();
	var second = datetime.getSeconds();
	var mseconds = datetime.getMilliseconds();
	return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":"
			+ second;
};
function timeStampToDate(time) {
	if (time == null || time == "") {
		return "";
	}
	var datetime = new Date();
	datetime.setTime(time);
	var year = datetime.getFullYear();
	var month = datetime.getMonth() + 1;
	var date = datetime.getDate();

	return year + "-" + month + "-" + date;
};
/**
 * 富文本组件
 */

function initKindEditor(objId){
	var basePath=$("#basePath").val();
	KindEditor.ready(function(K) { 
		var editor1 = K.create('textarea[id='+objId+']', {
			cssPath : basePath+'/style/admin/kindeditor/plugins/code/prettify.css',
			uploadJson : basePath+'/backgd/kindeditor/fileUpload',
			fileManagerJson : basePath+'/backgd/kindeditor/fileManager',
			allowFileManager : false, 
			afterCreate : function() {
				var self = this;
				K.ctrl(document, 13, function() {
					self.sync();
					//document.forms['example'].submit();
				});
				K.ctrl(self.edit.doc, 13, function() {
					self.sync();
					//document.forms['example'].submit();
				});
			}, 
			afterBlur:function(){ 
	          this.sync(); 
	        }    
		});
		prettyPrint();
	});
}

function uploadHead() {
	$.ajaxFileUpload({
		url : $("#basePath").val() + "/backgd/file/upLoad",// 需要链接到服务器地址
		secureuri : false,
		fileElementId : "basicInfoHead",// 文件选择框的id属性
		dataType : 'json', // json
		success : function(data) {
			$("#imgHead").attr("src",
					$("#basePath").val() + "/" + data.imagePath);
			$('#basicHeadUrl').val(data.imagePath);
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert('上传失败！');
		}
	});
}
function delData(url) {
	var ids = getIdSelections($('#cusTable'));

	if (ids.length == 0) {
		layer.msg("请选择要删除的信息！", {
			icon : 5
		});
	} else {
		layer.confirm('您确定要删除选中的数据吗?', {
			icon : 2,
			title : '提示'
		}, function(index) {
			sendAjax(url, {
				"ids" : ids + ""
			}, function(data) {
				if (data.success) {
					layer.msg(data.msg, {
						icon : 1
					});
					initTableData();
				} else {
					layer.msg(data.msg, {
						icon : 5
					});
				}
			});
		});
	}
}
/**
 * 列表页面删除单条数据
 * 
 * @param url
 */
function delDataItem(url) {

	layer.confirm('您确定要删除选中的数据吗?', {
		icon : 2,
		title : '提示'
	}, function(index) {
		sendAjax(url, {}, function(data) {
			if (data.success) {
				layer.msg(data.msg, {
					icon : 1
				});
				initTableData();
			} else {
				layer.msg(data.msg, {
					icon : 5
				});
			}
		});
	});

}
/**
 * 弹出提示框
 * 
 * @param tag(success、error)
 * @param msg
 */
function showToastr(tag, msg) {
	toastr.options = {
		"closeButton" : false,
		"debug" : false,
		"positionClass" : "toast-top-full-width",
		"onclick" : null,
		"showDuration" : "300",
		"hideDuration" : "1000",
		"timeOut" : "2000",
		"extendedTimeOut" : "1000",
		"showEasing" : "swing",
		"hideEasing" : "linear",
		"showMethod" : "fadeIn",
		"hideMethod" : "fadeOut"
	}
	toastr[tag](msg);
}

/**
 * 单图片上传
 * pickName 
 * displayName
 * fileName
 * basePath 
 */
function webUpload(pickName,displayName,fileName){
	var basePath=$("#basePath").val();
	// 初始化Web Uploader
	  var uploader = WebUploader.create({

	      // 选完文件后，是否自动上传。
	      auto: true,
	      // swf文件路径
	      swf:basePath+'/style/admin/webUploader/Uploader.swf',
	      // 文件接收服务端。
	      server: basePath+'/common/file/upLoad',
          chunked : true,
          duplicate:true,//是否可重复选择同一文件
          //fileNumLimit:1, 
	      // 选择文件的按钮。可选。
	      // 内部根据当前运行是创建，可能是input元素，也可能是flash.
	      pick: '#'+pickName,
	      // 只允许选择图片文件。
	      accept: {
	          title: 'Images',
	          extensions: 'gif,jpg,jpeg,bmp,png',
	          mimeTypes: 'image/jpg,image/jpeg,image/png'   //修改这行
	      }
	  });
	// 生成缩略图和上传进度
	  uploader.on("fileQueued", function(file) {
	          // 把文件信息追加到fileList的div中
		      $("#"+displayName).html("");
	          $("#"+displayName).append("<div id='" + file.id + "'><img/><div>"+file.name+"</div>" +
	          		"<div class=\"file-panel\" style=\"height: 20px;\"><a class=\"webremove\" href=\"javascript:webUploadRemove('"+file.id+"','"+fileName+"')\">删除</a></div> <div style=\"height: 4px;\"></div></div>")

	          // 制作缩略图
	          // error：不是图片，则有error
	          // src:代表生成缩略图的地址
	          uploader.makeThumb(file, function(error, src) {
	              if (error) {
	                  $("#" + file.id).find("img").replaceWith("<span>无法预览&nbsp;</span>");
	              } else {
	                  $("#" + file.id).find("img").attr("src", src);
	              }
	          });
	      }
	  );

	  // 监控上传进度
	  // percentage:代表上传文件的百分比
	  uploader.on("uploadProgress", function(file, percentage) {
	      //$("#" + file.id).find("span.percentage").text(Math.round(percentage * 100) + "%");
	  });
	  // 文件上传成功时候触发
	  uploader.on('uploadSuccess', function( file,response) {
//		   $("#" + file.id).find("[name='"+fileName+"']").val(response.path);
		   $("#" +fileName).val(response.path);
		   
		   //去除bootstrapValidator 
		   var bootstrapValidator = $("#myform").data('bootstrapValidator');  
		   bootstrapValidator.updateStatus(fileName, 'NOT_VALIDATED').validateField(fileName); //错误提示信息变了  
		   
	  });

}
/**
 * webUpload 单文件删除
 * @param fileId
 */
function webUploadRemove(fileId,fileName){
	 $("#" +fileId).remove();
	 $("#" +fileName).val("");  
}

/**
 * 多图片上传
 * pickName 
 * displayName
 * fileName
 * basePath 
 */
function webUploadMore(pickName,displayName,fileName){
	var basePath=$("#basePath").val();
	// 初始化Web Uploader
	  var uploader = WebUploader.create({

	      // 选完文件后，是否自动上传。
	      auto: true,
	      // swf文件路径
	      swf:basePath+'/style/admin/webUploader/Uploader.swf',
	      // 文件接收服务端。
	      server: basePath+'/common/file/upLoad',
          chunked : true,
          duplicate:true,//是否可重复选择同一文件
          //fileNumLimit:1, 
	      // 选择文件的按钮。可选。
	      // 内部根据当前运行是创建，可能是input元素，也可能是flash.
	      pick: '#'+pickName,
	      // 只允许选择图片文件。
	      accept: {
	          title: 'Images',
	          extensions: 'gif,jpg,jpeg,bmp,png',
	          mimeTypes: 'image/jpg,image/jpeg,image/png'   //修改这行
	      }
	  });
	// 生成缩略图和上传进度
	  uploader.on("fileQueued", function(file) {
	          // 把文件信息追加到fileList的div中 
	          $("#"+displayName).append("<div id='" + file.id + "' style=\"float: left;padding:3px\"><img/><div>"+file.name+"</div>" +
	          		"<div class=\"file-panel\" style=\"height: 20px;\"><a class=\"webremove\" href=\"javascript:webUploadMoreRemove('"+file.id+"')\">删除</a></div> <input type=\"hidden\" name='"+fileName+"'> <div style=\"height: 4px;\"></div></div>")

	          // 制作缩略图
	          // error：不是图片，则有error
	          // src:代表生成缩略图的地址
	          uploader.makeThumb(file, function(error, src) {
	              if (error) {
	                  $("#" + file.id).find("img").replaceWith("<span>无法预览&nbsp;</span>");
	              } else {
	                  $("#" + file.id).find("img").attr("src", src);
	              }
	          });
	      }
	  );

	  // 监控上传进度
	  // percentage:代表上传文件的百分比
	  uploader.on("uploadProgress", function(file, percentage) {
	      //$("#" + file.id).find("span.percentage").text(Math.round(percentage * 100) + "%");
	  });
	  // 文件上传成功时候触发
	  uploader.on('uploadSuccess', function( file,response) {
 		   $("#" + file.id).find("[name='"+fileName+"']").val(response.path);
	  });

}
/**
 * webUpload 删除
 * @param fileId
 */
function webUploadMoreRemove(fileId){
	 $("#" +fileId).remove();
}
/**
 * webUpload 删除
 * @param fileId
 */
function fileRemove(divid,fileId){
	 $("#" +divid).remove();
	 $("#" +fileId).val();
}