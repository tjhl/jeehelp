$(document).ready(function() {
	var i = -1;
	var toastCount = 0;
	var $toastlast;
 
	function showToastr() {
		var shortCutFunction = "success";
		var msg = $('#message').val();
		var title = $('#title').val() || '';
		var $showDuration = $('#showDuration');
		var $hideDuration = $('#hideDuration');
		var $timeOut = $('#timeOut');
		var $extendedTimeOut = $('#extendedTimeOut');
		var $showEasing = $('#showEasing');
		var $hideEasing = $('#hideEasing');
		var $showMethod = $('#showMethod');
		var $hideMethod = $('#hideMethod');
		var toastIndex = toastCount++;

		toastr.options = {
				  "closeButton": false,
				  "debug": false,
				  "positionClass": "toast-top-full-width",
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				}
  
		$("#toastrOptions").text("Command: toastr["
						+ shortCutFunction
						+ "](\""
						+ msg
						+ (title ? "\", \"" + title : '')
						+ "\")\n\ntoastr.options = "
						+ JSON.stringify(toastr.options, null, 2)
		);

		var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
		$toastlast = $toast;
		if ($toast.find('#okBtn').length) {
			$toast.delegate('#okBtn', 'click', function () {
				alert('you clicked me. i was toast #' + toastIndex + '. goodbye!');
				$toast.remove();
			});
		}
		if ($toast.find('#surpriseBtn').length) {
			$toast.delegate('#surpriseBtn', 'click', function () {
				alert('Surprise! you clicked me. i was toast #' + toastIndex + '. You could perform an action here.');
			});
		}
	};
	function getLastToast(){
		return $toastlast;
	}
	$('#clearlasttoast').click(function () {
		toastr.clear(getLastToast());
	});
	$('#cleartoasts').click(function () {
		toastr.clear();
	});
})