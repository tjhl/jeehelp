/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : jeehelp

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-08-07 22:12:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `common_file`
-- ----------------------------
DROP TABLE IF EXISTS `common_file`;
CREATE TABLE `common_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_path` varchar(255) DEFAULT NULL COMMENT '文件路径',
  `file_name` varchar(255) DEFAULT NULL,
  `table_id` bigint(20) DEFAULT NULL COMMENT '对应的表里面的id',
  `table_name` varchar(255) DEFAULT NULL COMMENT '对应的那个表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='公用附件表';

-- ----------------------------
-- Records of common_file
-- ----------------------------
INSERT INTO `common_file` VALUES ('30', '/fileupload/20170609085436861862.docx', '问题沟通反馈.docx', '104', 'project_work_report');

-- ----------------------------
-- Table structure for `content_news`
-- ----------------------------
DROP TABLE IF EXISTS `content_news`;
CREATE TABLE `content_news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `pic_url` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `remark` text COMMENT '内容',
  `isdel` int(255) DEFAULT '0' COMMENT '是否删除0未删除 1删除',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(255) DEFAULT NULL COMMENT '创建人',
  `modified_date` datetime DEFAULT NULL COMMENT '修改时间',
  `modified_user` bigint(20) DEFAULT NULL COMMENT '修改人',
  `type` varchar(50) DEFAULT NULL COMMENT '类型 字典表 对应字典类型content_type',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章内容';

-- ----------------------------
-- Records of content_news
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_blob_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('org.springframework.scheduling.quartz.SchedulerFactoryBean#0', 'cronTrigger', 'DEFAULT', '*/10 * * ? * *', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for `qrtz_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------
INSERT INTO `qrtz_fired_triggers` VALUES ('org.springframework.scheduling.quartz.SchedulerFactoryBean#0', 'USER-20131209ZD15010506862261501050686200', 'cronTrigger', 'DEFAULT', 'USER-20131209ZD1501050686226', '1501050720119', '1501050730000', '0', 'ACQUIRED', null, null, '0', '0');

-- ----------------------------
-- Table structure for `qrtz_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('org.springframework.scheduling.quartz.SchedulerFactoryBean#0', 'firstComplexJobDetail', 'DEFAULT', null, 'com.crht.task.MyTask', '1', '0', '0', '1', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787000737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F40000000000010770800000010000000007800);

-- ----------------------------
-- Table structure for `qrtz_locks`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('org.springframework.scheduling.quartz.SchedulerFactoryBean#0', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('org.springframework.scheduling.quartz.SchedulerFactoryBean#0', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for `qrtz_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('org.springframework.scheduling.quartz.SchedulerFactoryBean#0', 'USER-20131209ZD1501050686226', '1501050711971', '20000');

-- ----------------------------
-- Table structure for `qrtz_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('org.springframework.scheduling.quartz.SchedulerFactoryBean#0', 'cronTrigger', 'DEFAULT', 'firstComplexJobDetail', 'DEFAULT', null, '1501050730000', '1501050720000', '0', 'ACQUIRED', 'CRON', '1501050686000', '0', null, '0', '');

-- ----------------------------
-- Table structure for `system_dict`
-- ----------------------------
DROP TABLE IF EXISTS `system_dict`;
CREATE TABLE `system_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '名称',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(255) DEFAULT NULL COMMENT '创建人',
  `operation_date` datetime DEFAULT NULL COMMENT '操作时间',
  `operation_user` bigint(255) DEFAULT NULL COMMENT '操作人',
  `type_key` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '关键词',
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '字典值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1 COMMENT='字典';

-- ----------------------------
-- Records of system_dict
-- ----------------------------
INSERT INTO `system_dict` VALUES ('40', '男', null, null, null, null, 'gender', '0');
INSERT INTO `system_dict` VALUES ('41', '女', null, null, null, null, 'gender\n', '1');
INSERT INTO `system_dict` VALUES ('42', '最新咨询', '2017-07-27 14:46:28', null, null, null, 'content_type', '0');
INSERT INTO `system_dict` VALUES ('43', '历史文章', '2017-07-27 14:46:47', null, null, null, 'content_type', '1');

-- ----------------------------
-- Table structure for `system_log`
-- ----------------------------
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL COMMENT '访问地址',
  `ip` varchar(255) DEFAULT NULL COMMENT 'ip地址',
  `remark` varchar(255) DEFAULT NULL,
  `user_browser` varchar(255) DEFAULT NULL COMMENT '用户浏览器',
  `user_os` varchar(255) DEFAULT NULL COMMENT '操作系统',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `create_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台系统日志记录';

-- ----------------------------
-- Records of system_log
-- ----------------------------

-- ----------------------------
-- Table structure for `system_resources`
-- ----------------------------
DROP TABLE IF EXISTS `system_resources`;
CREATE TABLE `system_resources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '0' COMMENT '类型 0 菜单 1按钮',
  `pid` bigint(20) DEFAULT '0' COMMENT '自关联上级ID',
  `url` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL COMMENT '权限标示',
  `sort` int(11) DEFAULT '0',
  `level` int(11) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_user` bigint(20) DEFAULT NULL COMMENT '修改用户',
  `modified_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COMMENT='系统菜单';

-- ----------------------------
-- Records of system_resources
-- ----------------------------
INSERT INTO `system_resources` VALUES ('32', '基础设置', '0', '0', null, null, '0', '1', null, null, null, null);
INSERT INTO `system_resources` VALUES ('34', '系统管理', '0', '0', null, null, '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('35', '角色管理', '0', '34', 'backgd/system/systemRole/toList', '', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('36', '用户管理', '0', '34', 'backgd/system/systemUser/toList', null, '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('37', '修改密码', '0', '34', 'backgd/system/systemHome/updatePwd', null, '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('39', '添加角色', '1', '35', '', 'system:systemRole:add', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('40', '编辑角色', '1', '35', null, 'system:systemRole:update', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('41', '角色权限', '1', '35', null, 'system:systemRole:updateRoleResource', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('42', '添加管理员', '1', '36', null, 'system:systemUser:add', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('43', '编辑管理员', '1', '36', null, 'system:systemUser:update', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('44', '删除角色', '1', '35', null, 'system:systemRole:delete', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('49', '删除管理员', '1', '36', null, 'system:systemUser:delete', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('84', '字典管理', '0', '32', 'backgd/system/dict/toList', null, '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('85', '列表', '1', '35', null, 'system:systemRole:list', '0', '2', null, null, null, null);
INSERT INTO `system_resources` VALUES ('143', '列表', '1', '36', null, 'system:systemUser:list', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('144', '字典列表', '1', '84', null, 'system:dict:list', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('145', '字典添加', '1', '84', null, 'system:dict:add', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('146', '字典修改', '1', '84', null, 'system:dict:update', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('147', '字典删除', '1', '84', null, 'system:dict:delete', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('148', '平台设置', '0', '32', 'backgd/system/systemWebsite/toUpdate', null, '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('149', '平台设置更新', '1', '148', null, 'system:systemWebsite:update', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('150', '修改更新密码', '1', '37', null, 'system:systemHome:updatePwdServer', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('151', '菜单管理', '0', '34', 'backgd/system/menu/menulist', null, '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('152', '查询', '1', '151', null, 'system:menu:getAllResource', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('154', '添加', '1', '151', '', 'system:menu:addResource', '3', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('155', '修改', '1', '151', null, 'system:menu:updateResource', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('156', '信息管理', '0', '0', null, null, '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('157', '删除', '1', '151', null, 'system:menu:deletResource', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('158', '文章管理', '0', '156', 'backgd/content/contentNews/toList', '', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('159', '查询', '1', '158', '', 'content:contentNews:list', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('160', '添加', '1', '158', '', 'content:contentNews:add', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('161', '修改', '1', '158', '', 'content:contentNews:update', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('162', '删除', '1', '158', '', 'content:contentNews:delete', '0', null, null, null, null, null);
INSERT INTO `system_resources` VALUES ('164', 'excel导入', '0', '156', 'backgd/content/excel/toAdd', '', '0', null, null, null, null, null);

-- ----------------------------
-- Table structure for `system_role`
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `isdel` int(11) DEFAULT '0' COMMENT '是否删除0 正常 1删除',
  `modified_date` datetime DEFAULT NULL COMMENT '修改时间',
  `modified_user` bigint(20) DEFAULT NULL COMMENT '修改人',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `create_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='系统角色';

-- ----------------------------
-- Records of system_role
-- ----------------------------
INSERT INTO `system_role` VALUES ('1', '超级管理员', null, '0', '2017-07-03 22:18:05', null, null, null);
INSERT INTO `system_role` VALUES ('23', '普通管理员', '', '0', '2017-07-03 23:04:08', null, null, '2017-07-03 22:57:28');
INSERT INTO `system_role` VALUES ('24', '项目经理', '', '0', '2017-07-04 23:33:37', null, null, '2017-07-03 23:04:23');
INSERT INTO `system_role` VALUES ('25', '33', '3', '0', null, null, null, '2017-08-07 22:07:22');
INSERT INTO `system_role` VALUES ('26', '4', '5', '0', null, null, null, '2017-08-07 22:07:47');
INSERT INTO `system_role` VALUES ('27', '4', '4', '0', null, null, null, '2017-08-07 22:08:03');
INSERT INTO `system_role` VALUES ('28', '5', '5', '0', null, null, null, '2017-08-07 22:08:31');

-- ----------------------------
-- Table structure for `system_role_resources`
-- ----------------------------
DROP TABLE IF EXISTS `system_role_resources`;
CREATE TABLE `system_role_resources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `resources_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1414 DEFAULT CHARSET=utf8 COMMENT='系统权限中间表';

-- ----------------------------
-- Records of system_role_resources
-- ----------------------------
INSERT INTO `system_role_resources` VALUES ('1200', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1201', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1202', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1203', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1204', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1205', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1206', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1207', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1208', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1209', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1210', '1', '32');
INSERT INTO `system_role_resources` VALUES ('1211', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1212', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1213', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1214', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1215', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1216', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1217', '1', '85');
INSERT INTO `system_role_resources` VALUES ('1218', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1219', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1220', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1221', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1222', '1', '32');
INSERT INTO `system_role_resources` VALUES ('1223', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1224', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1225', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1226', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1227', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1228', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1229', '1', '85');
INSERT INTO `system_role_resources` VALUES ('1230', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1231', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1232', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1233', '1', '49');
INSERT INTO `system_role_resources` VALUES ('1234', '1', '143');
INSERT INTO `system_role_resources` VALUES ('1235', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1236', '1', '32');
INSERT INTO `system_role_resources` VALUES ('1237', '1', '84');
INSERT INTO `system_role_resources` VALUES ('1238', '1', '144');
INSERT INTO `system_role_resources` VALUES ('1239', '1', '145');
INSERT INTO `system_role_resources` VALUES ('1240', '1', '146');
INSERT INTO `system_role_resources` VALUES ('1241', '1', '147');
INSERT INTO `system_role_resources` VALUES ('1242', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1243', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1244', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1245', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1246', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1247', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1248', '1', '85');
INSERT INTO `system_role_resources` VALUES ('1249', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1250', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1251', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1252', '1', '49');
INSERT INTO `system_role_resources` VALUES ('1253', '1', '143');
INSERT INTO `system_role_resources` VALUES ('1254', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1255', '1', '32');
INSERT INTO `system_role_resources` VALUES ('1256', '1', '84');
INSERT INTO `system_role_resources` VALUES ('1257', '1', '144');
INSERT INTO `system_role_resources` VALUES ('1258', '1', '145');
INSERT INTO `system_role_resources` VALUES ('1259', '1', '146');
INSERT INTO `system_role_resources` VALUES ('1260', '1', '147');
INSERT INTO `system_role_resources` VALUES ('1261', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1262', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1263', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1264', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1265', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1266', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1267', '1', '85');
INSERT INTO `system_role_resources` VALUES ('1268', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1269', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1270', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1271', '1', '49');
INSERT INTO `system_role_resources` VALUES ('1272', '1', '143');
INSERT INTO `system_role_resources` VALUES ('1273', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1274', '1', '32');
INSERT INTO `system_role_resources` VALUES ('1275', '1', '84');
INSERT INTO `system_role_resources` VALUES ('1276', '1', '144');
INSERT INTO `system_role_resources` VALUES ('1277', '1', '145');
INSERT INTO `system_role_resources` VALUES ('1278', '1', '146');
INSERT INTO `system_role_resources` VALUES ('1279', '1', '147');
INSERT INTO `system_role_resources` VALUES ('1280', '1', '148');
INSERT INTO `system_role_resources` VALUES ('1281', '1', '149');
INSERT INTO `system_role_resources` VALUES ('1282', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1283', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1284', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1285', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1286', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1287', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1288', '1', '85');
INSERT INTO `system_role_resources` VALUES ('1289', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1290', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1291', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1292', '1', '49');
INSERT INTO `system_role_resources` VALUES ('1293', '1', '143');
INSERT INTO `system_role_resources` VALUES ('1294', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1295', '1', '150');
INSERT INTO `system_role_resources` VALUES ('1296', '1', '32');
INSERT INTO `system_role_resources` VALUES ('1297', '1', '84');
INSERT INTO `system_role_resources` VALUES ('1298', '1', '144');
INSERT INTO `system_role_resources` VALUES ('1299', '1', '145');
INSERT INTO `system_role_resources` VALUES ('1300', '1', '146');
INSERT INTO `system_role_resources` VALUES ('1301', '1', '147');
INSERT INTO `system_role_resources` VALUES ('1302', '1', '148');
INSERT INTO `system_role_resources` VALUES ('1303', '1', '149');
INSERT INTO `system_role_resources` VALUES ('1304', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1305', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1306', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1307', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1308', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1309', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1310', '1', '85');
INSERT INTO `system_role_resources` VALUES ('1311', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1312', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1313', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1314', '1', '49');
INSERT INTO `system_role_resources` VALUES ('1315', '1', '143');
INSERT INTO `system_role_resources` VALUES ('1316', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1317', '1', '150');
INSERT INTO `system_role_resources` VALUES ('1318', '1', '151');
INSERT INTO `system_role_resources` VALUES ('1319', '1', '152');
INSERT INTO `system_role_resources` VALUES ('1320', '1', '32');
INSERT INTO `system_role_resources` VALUES ('1321', '1', '84');
INSERT INTO `system_role_resources` VALUES ('1322', '1', '144');
INSERT INTO `system_role_resources` VALUES ('1323', '1', '145');
INSERT INTO `system_role_resources` VALUES ('1324', '1', '146');
INSERT INTO `system_role_resources` VALUES ('1325', '1', '147');
INSERT INTO `system_role_resources` VALUES ('1326', '1', '148');
INSERT INTO `system_role_resources` VALUES ('1327', '1', '149');
INSERT INTO `system_role_resources` VALUES ('1328', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1329', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1330', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1331', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1332', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1333', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1334', '1', '85');
INSERT INTO `system_role_resources` VALUES ('1335', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1336', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1337', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1338', '1', '49');
INSERT INTO `system_role_resources` VALUES ('1339', '1', '143');
INSERT INTO `system_role_resources` VALUES ('1340', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1341', '1', '150');
INSERT INTO `system_role_resources` VALUES ('1342', '1', '151');
INSERT INTO `system_role_resources` VALUES ('1343', '1', '152');
INSERT INTO `system_role_resources` VALUES ('1344', '1', '154');
INSERT INTO `system_role_resources` VALUES ('1345', '1', '155');
INSERT INTO `system_role_resources` VALUES ('1346', '1', '157');
INSERT INTO `system_role_resources` VALUES ('1347', '1', '32');
INSERT INTO `system_role_resources` VALUES ('1348', '1', '84');
INSERT INTO `system_role_resources` VALUES ('1349', '1', '144');
INSERT INTO `system_role_resources` VALUES ('1350', '1', '145');
INSERT INTO `system_role_resources` VALUES ('1351', '1', '146');
INSERT INTO `system_role_resources` VALUES ('1352', '1', '147');
INSERT INTO `system_role_resources` VALUES ('1353', '1', '148');
INSERT INTO `system_role_resources` VALUES ('1354', '1', '149');
INSERT INTO `system_role_resources` VALUES ('1355', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1356', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1357', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1358', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1359', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1360', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1361', '1', '85');
INSERT INTO `system_role_resources` VALUES ('1362', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1363', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1364', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1365', '1', '49');
INSERT INTO `system_role_resources` VALUES ('1366', '1', '143');
INSERT INTO `system_role_resources` VALUES ('1367', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1368', '1', '150');
INSERT INTO `system_role_resources` VALUES ('1369', '1', '151');
INSERT INTO `system_role_resources` VALUES ('1370', '1', '152');
INSERT INTO `system_role_resources` VALUES ('1371', '1', '154');
INSERT INTO `system_role_resources` VALUES ('1372', '1', '155');
INSERT INTO `system_role_resources` VALUES ('1373', '1', '157');
INSERT INTO `system_role_resources` VALUES ('1374', '1', '156');
INSERT INTO `system_role_resources` VALUES ('1375', '1', '158');
INSERT INTO `system_role_resources` VALUES ('1376', '1', '159');
INSERT INTO `system_role_resources` VALUES ('1377', '1', '160');
INSERT INTO `system_role_resources` VALUES ('1378', '1', '161');
INSERT INTO `system_role_resources` VALUES ('1379', '1', '162');
INSERT INTO `system_role_resources` VALUES ('1380', '1', '32');
INSERT INTO `system_role_resources` VALUES ('1381', '1', '84');
INSERT INTO `system_role_resources` VALUES ('1382', '1', '144');
INSERT INTO `system_role_resources` VALUES ('1383', '1', '145');
INSERT INTO `system_role_resources` VALUES ('1384', '1', '146');
INSERT INTO `system_role_resources` VALUES ('1385', '1', '147');
INSERT INTO `system_role_resources` VALUES ('1386', '1', '148');
INSERT INTO `system_role_resources` VALUES ('1387', '1', '149');
INSERT INTO `system_role_resources` VALUES ('1388', '1', '34');
INSERT INTO `system_role_resources` VALUES ('1389', '1', '35');
INSERT INTO `system_role_resources` VALUES ('1390', '1', '39');
INSERT INTO `system_role_resources` VALUES ('1391', '1', '40');
INSERT INTO `system_role_resources` VALUES ('1392', '1', '41');
INSERT INTO `system_role_resources` VALUES ('1393', '1', '44');
INSERT INTO `system_role_resources` VALUES ('1394', '1', '85');
INSERT INTO `system_role_resources` VALUES ('1395', '1', '36');
INSERT INTO `system_role_resources` VALUES ('1396', '1', '42');
INSERT INTO `system_role_resources` VALUES ('1397', '1', '43');
INSERT INTO `system_role_resources` VALUES ('1398', '1', '49');
INSERT INTO `system_role_resources` VALUES ('1399', '1', '143');
INSERT INTO `system_role_resources` VALUES ('1400', '1', '37');
INSERT INTO `system_role_resources` VALUES ('1401', '1', '150');
INSERT INTO `system_role_resources` VALUES ('1402', '1', '151');
INSERT INTO `system_role_resources` VALUES ('1403', '1', '152');
INSERT INTO `system_role_resources` VALUES ('1404', '1', '154');
INSERT INTO `system_role_resources` VALUES ('1405', '1', '155');
INSERT INTO `system_role_resources` VALUES ('1406', '1', '157');
INSERT INTO `system_role_resources` VALUES ('1407', '1', '156');
INSERT INTO `system_role_resources` VALUES ('1408', '1', '158');
INSERT INTO `system_role_resources` VALUES ('1409', '1', '159');
INSERT INTO `system_role_resources` VALUES ('1410', '1', '160');
INSERT INTO `system_role_resources` VALUES ('1411', '1', '161');
INSERT INTO `system_role_resources` VALUES ('1412', '1', '162');
INSERT INTO `system_role_resources` VALUES ('1413', '1', '164');

-- ----------------------------
-- Table structure for `system_user`
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `user_pass` varchar(255) DEFAULT NULL,
  `real_name` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `state` int(11) DEFAULT '1' COMMENT '1可用 2停用',
  `remark` varchar(500) DEFAULT NULL,
  `role` bigint(11) DEFAULT NULL COMMENT '角色',
  `isdel` int(11) DEFAULT '0' COMMENT '是否删除0 正常 1删除',
  `last_login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(255) DEFAULT NULL COMMENT '最后登录ip',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `create_date` datetime DEFAULT NULL COMMENT '修改时间',
  `modified_user` bigint(20) DEFAULT NULL COMMENT '修改用户',
  `modified_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO `system_user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '管理员', '1', null, '1', '0', '2017-07-03 22:12:56', null, null, null, null, '2017-07-03 23:26:24');

-- ----------------------------
-- Table structure for `system_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE `system_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='用户角色关系表';

-- ----------------------------
-- Records of system_user_role
-- ----------------------------
INSERT INTO `system_user_role` VALUES ('1', '1', '1', null, null);
INSERT INTO `system_user_role` VALUES ('2', '3', '23', '1', '2017-07-23 22:36:06');
INSERT INTO `system_user_role` VALUES ('3', '3', '24', '1', '2017-07-23 22:36:06');
INSERT INTO `system_user_role` VALUES ('8', '4', '1', null, '2017-07-23 23:00:32');
INSERT INTO `system_user_role` VALUES ('9', '4', '23', null, '2017-07-23 23:00:32');
INSERT INTO `system_user_role` VALUES ('10', '4', '24', null, '2017-07-23 23:00:32');

-- ----------------------------
-- Table structure for `system_website`
-- ----------------------------
DROP TABLE IF EXISTS `system_website`;
CREATE TABLE `system_website` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) DEFAULT NULL COMMENT '系统logo',
  `title` varchar(255) DEFAULT NULL COMMENT '网站名称',
  `company` varchar(255) DEFAULT NULL COMMENT '公司名称',
  `beian` varchar(255) DEFAULT NULL COMMENT '备案号',
  `phone` varchar(255) DEFAULT NULL COMMENT '客服电话',
  `qq` varchar(255) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL COMMENT '版权',
  `create_user` bigint(20) DEFAULT NULL COMMENT '创建用户',
  `create_date` datetime DEFAULT NULL COMMENT '修改时间',
  `modified_user` bigint(20) DEFAULT NULL COMMENT '修改用户',
  `modified_date` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='网站信息';

-- ----------------------------
-- Records of system_website
-- ----------------------------
INSERT INTO `system_website` VALUES ('1', null, 'XX综合管理系统', 'xx科技有限公司', '津ICP备', '18807644587', '1540354312', '&copy;2010 - 2018 xx', null, null, null, '2017-07-16 23:21:43');
